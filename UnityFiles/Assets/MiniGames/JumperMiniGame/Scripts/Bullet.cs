﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody2D bullet;
    private float bulletSpeed = 4.0f;
    private float bulletStartPoint = 1.9f;

    void Start()
    {
        bullet = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        bullet.velocity = Vector2.left * bulletSpeed;
        if (bullet.transform.position.x < -4)
        {
            transform.position = new Vector2(bulletStartPoint, transform.position.y);
        }
    }
}
