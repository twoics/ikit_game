﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnChunks : MonoBehaviour
{
    public Transform Player;
    public Chunk[] ChunksPrefabs;
    public Chunk FirstChunk;

    private List<Chunk> SpawnedChunks = new List<Chunk>();

    void Start()
    {
        SpawnedChunks.Add(FirstChunk);
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.position.y > SpawnedChunks[SpawnedChunks.Count - 1].End.position.y - 8)
            SpawnChunk();
    }
    private void SpawnChunk()
    {
        Chunk NewChunk = Instantiate(ChunksPrefabs[Random.Range(0, ChunksPrefabs.Length)]);
        NewChunk.transform.position = SpawnedChunks[SpawnedChunks.Count - 1].End.position - NewChunk.Begin.localPosition;
        SpawnedChunks.Add(NewChunk);

        if(SpawnedChunks.Count >= 3)
        {
            Destroy(SpawnedChunks[0].gameObject);
            SpawnedChunks.RemoveAt(0);
        }
    }
}
