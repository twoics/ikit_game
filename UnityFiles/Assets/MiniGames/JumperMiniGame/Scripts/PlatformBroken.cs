using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBroken : MonoBehaviour
{
    public GameObject player;
    public GameObject platformItself;
    public GameObject physics;

    private bool checkerForPhysics;
    private float minimalStep = 0.5f;

    void Update()
    {
        if (player.transform.position.y > transform.position.y + minimalStep)
        {
            checkerForPhysics = true;
        }
        else if (player.transform.position.y < transform.position.y)
        {
            checkerForPhysics = false;
        }

        if (checkerForPhysics)
        {
            physics.SetActive(true);
        }
        else
        {
            physics.SetActive(false);
        }
    }
}
