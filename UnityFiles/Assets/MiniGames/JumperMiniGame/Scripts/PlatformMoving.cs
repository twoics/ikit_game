﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMoving : MonoBehaviour
{
    public GameObject player;
    public GameObject physics;

    private bool checkerForPhysics;
    private float minimalStep = 0.5f;
    private float platformSpeed = 0.01f;

    void Update()
    {
        if ((physics.transform.position.x >= 2) || (physics.transform.position.x <= -2))
        {
            platformSpeed *= -1;
        }
        transform.position = new Vector2(physics.transform.position.x + platformSpeed, physics.transform.position.y);
        physics.transform.position = new Vector2(physics.transform.position.x + platformSpeed, physics.transform.position.y);

        if (player.transform.position.y > transform.position.y + minimalStep)
        {
            checkerForPhysics = true;
        }
        else if (player.transform.position.y < transform.position.y)
        {
            checkerForPhysics = false;
        }

        if (checkerForPhysics)
        {
            physics.SetActive(true);
        }
        else
        {
            physics.SetActive(false);
        }
    }
}
