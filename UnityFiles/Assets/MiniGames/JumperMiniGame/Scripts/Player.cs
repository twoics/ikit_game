using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public GameObject cam;
    public GameObject playerStandartPicture;
    public GameObject playerCapPicture;
    public GameObject playerRocketPicture;

    public GameObject cap1;
    public GameObject rocket1;
    public GameObject platformOneTime1;
    public GameObject platformOneTime2;
    public GameObject platformOneTime3;
    public GameObject platformBroken1;
    public GameObject platformBroken2;
    public GameObject platformBroken3;

    public Text scoreAttemptCounter;
    public Text scoreTotalCounter;

    private Rigidbody2D player;

    private float flightTime;
    private float flightSpeed;
    private float jumpFlightSpeed = 5.5f;
    private float trampolineFlightSpeed = 15.0f;
    private float jumpFlightTime = 0.8f;
    private float trampolineFlightTime = 0.8f;
    private float startingPointCorrection = 146.0f;
    private float sideStep = 6.0f;
    private float sideBoarder = 2.4f;

    private float horizontalInput;
    private float scoreAttempt;
    private float scoreTotal;

    void Start()
    {
        player = GetComponent<Rigidbody2D>();
        scoreAttempt = 0;
        scoreTotal = 0;
        flightTime = 0.0f;
        player.AddForce(transform.up * 400);
    }

    void FixedUpdate()
    {
        if (flightTime > 0)
        {
            player.velocity = Vector2.up * flightSpeed;
            flightTime -= Time.deltaTime;
            if (flightTime <= 0)
            {
                playerStandartPicture.SetActive(true);
                player.gravityScale = 1;
            }
        }

        if (player.transform.position.y + startingPointCorrection > scoreAttempt)
        {
            scoreAttempt = player.transform.position.y + startingPointCorrection;
        }
        if(scoreTotal < scoreAttempt)
        {
            scoreTotal = scoreAttempt;
        }
        scoreAttemptCounter.text = scoreAttempt.ToString("0");
        scoreTotalCounter.text = scoreTotal.ToString("0");

        if (Application.platform == RuntimePlatform.Android)
        {
            horizontalInput = Input.acceleration.x;         
        }
        else
        {
            horizontalInput = Input.GetAxis("Horizontal");
        }

        player.velocity = new Vector2 (horizontalInput * sideStep, player.velocity.y);

        if (transform.position.x > sideBoarder)
        {
            player.transform.position = new Vector3(-sideBoarder, player.transform.position.y, player.transform.position.z);
        }
        if (transform.position.x < -sideBoarder)
        {
            player.transform.position = new Vector3(sideBoarder, player.transform.position.y, player.transform.position.z);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlatformStandart")
        {
            StartCoroutine(StartAnimation());
            flightTime = jumpFlightTime;
            flightSpeed = jumpFlightSpeed;
        }

        if (collision.gameObject.tag == "PlatformOneTime")
        {
            StartCoroutine(StartAnimation());
            flightTime = jumpFlightTime;
            flightSpeed = jumpFlightSpeed;
            Destroy(collision.transform.parent.gameObject);
        }

        if (collision.gameObject.tag == "PlatformBroken")
        {
            Destroy(collision.transform.parent.gameObject);

        }
      

        if (collision.gameObject.tag == "Trampoline")
        {
            flightTime = trampolineFlightTime;
            flightSpeed = trampolineFlightSpeed;
        }


        if ((collision.gameObject.tag == "DeathBlock") || (collision.gameObject.tag == "Bullet"))
        {
            scoreTotal += scoreAttempt;

            SceneManager.LoadScene("Jumper");
        }
    }
    
    private IEnumerator StartAnimation()
    {
        playerStandartPicture.GetComponent<Animator>().SetBool("IsPlatformTouch", true);

        yield return new WaitForSeconds(1f);

        playerStandartPicture.GetComponent<Animator>().SetBool("IsPlatformTouch", false);

    }

    public void ExitGame()
    {
        PlayerPrefs.SetInt("CountMoneyFromMiniGames", (int)scoreTotal);
        SceneManager.LoadScene("SampleScene");
    }
}
