﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BodyTrigger : MonoBehaviour
{
    public GameObject snowboarder;
    public GameObject Lose;


    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            Debug.Log("Земля тело");
            //if (snowboarder)
            //{
                Lose.SetActive(true);
                Destroy(snowboarder);
            //}
        }

        if ((collision.gameObject.tag == "fire") || (collision.gameObject.tag == "thorns"))
        {
            Debug.Log("Огонь или шипы тело");
            //if (snowboarder)
            //{
                Lose.SetActive(true);
                Destroy(snowboarder);
            //}
        }

        if ((collision.gameObject.tag == "coin10") || (collision.gameObject.tag == "coin20") || (collision.gameObject.tag == "coin50") || (collision.gameObject.tag == "coin100"))
        {
            Debug.Log("Деньги тело");
            if (collision.gameObject.tag == "coin10")
            {
                gameObject.GetComponentInParent<Snowboarder>().money += 10;
            }
            if (collision.gameObject.tag == "coin20")
            {
                gameObject.GetComponentInParent<Snowboarder>().money += 20;
            }
            if (collision.gameObject.tag == "coin50")
            {
                gameObject.GetComponentInParent<Snowboarder>().money += 50;
            }
            if (collision.gameObject.tag == "coin100")
            {
                gameObject.GetComponentInParent<Snowboarder>().money += 100;
            }

            Destroy(collision.gameObject);
        }
    }
}
