﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam : MonoBehaviour
{
    public GameObject snowboarder;
    private float positionX; // X координата камеры
    private float positionY; // Y координата камеры
    private float offsetX = 8.7f; // Смещение камеры относительно сноубордиста по X координате
    private int offsetY = -2; // Смещение камеры относительно сноубордиста по Y координате
    private int positionZ = -10; // Z координата камеры

    void Update()
    {
        // Смещение камеры относительно сноубордиста по X координате
        positionX = snowboarder.transform.position.x + offsetX;
        // Смещение камеры относительно сноубордиста по X координате
        positionY = snowboarder.transform.position.y + offsetY;
        // Перемещение камеры при движении сноубордиста
        transform.position = new Vector3(positionX, positionY, positionZ);
    }
}
