﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MoveCamera : MonoBehaviour
{
    // Limit to move camera
    [SerializeField] float LeftLimit;
    [SerializeField] float RightLimit;
    [SerializeField] float BottomLimit;
    [SerializeField] float UpLimit;

    Vector3 ClickPosition = Vector3.zero;
    Vector3 NowPosition = Vector3.zero;
    Vector3 CameraPosition = Vector3.zero;

    private Tilemap Map;
    private Camera MainCamera;
    private float ZoomMin = 0.5f;
    private float ZoomMax = 1.8f;

    public MainParameters main_parameters;
    void Start()
    {
        Map = GetComponent<Tilemap>();
        MainCamera = Camera.main;
    }

    void Update()
    {
        if (main_parameters.IsMainWindowActive == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ClickPosition = Input.mousePosition;
                CameraPosition = transform.position;

            }
            if(Input.touchCount == 2)
            {
                Touch TouchZero = Input.GetTouch(0);
                Touch TouchOne = Input.GetTouch(1);

                Vector2 TouchZeroLastPos = TouchZero.position - TouchZero.deltaPosition;
                Vector2 TouchOneLastPos = TouchOne.position - TouchOne.deltaPosition;

                float DistTouch = (TouchZeroLastPos - TouchOneLastPos).magnitude;
                float CurrentDistTouch = (TouchZero.position - TouchOne.position).magnitude;

                float Difference = CurrentDistTouch - DistTouch;
                Zoom(Difference * 0.005f);

            }

            else if (Input.GetMouseButton(0))
            {
                NowPosition = Input.mousePosition;
                Vector3 direction = Camera.main.ScreenToWorldPoint(NowPosition) - Camera.main.ScreenToWorldPoint(ClickPosition);
                direction = direction * -1;
                Vector3 position = CameraPosition + direction;
                transform.position = position;
            }

            transform.position = new Vector3
                (
                Mathf.Clamp(transform.position.x, LeftLimit, RightLimit),
                Mathf.Clamp(transform.position.y, BottomLimit, UpLimit),
                -8
                );
        }
    }

    void Zoom(float Increment)
    {
        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - Increment, ZoomMin, ZoomMax);
    }

}