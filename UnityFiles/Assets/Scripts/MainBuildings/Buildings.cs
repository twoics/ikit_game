﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;
using System.IO;
using System;
using Random = UnityEngine.Random;

public abstract class Buildings : MainParameters
{
    public struct Teacher
    {
        public string TeacherName;
        public string TeacherSurname;
        public int TeacherStudentsBonus;
        public int TeacherWage;
        public int TeacherRatingBonus;
        public string TeacherDescription;

        public Teacher(string StructName, string StructSurname, string StructDescription, int StructStudentsBonus, int StructWage, int StructRatingBonus)
        {
            /*
            Initialization
                :param Name: Teacher Name
                :param StudentsImpact: Impact on students after accept
                :param TeacherSalary: Salary, by it's amount each month will be deducted from the amount of money
                :param RetingImpact: Impact on reting after accept
                :param TeacherDescription: Description
             */
            this.TeacherName = StructName;
            this.TeacherSurname = StructSurname;
            this.TeacherDescription = StructDescription;
            this.TeacherStudentsBonus = StructStudentsBonus;
            this.TeacherWage = StructWage;
            this.TeacherRatingBonus = StructRatingBonus;
        }
    }

    // Building Tiles in different state
    public TileBase TileBefore;
    public TileBase TileToSet;
    public TileBase TileHostelTwo;
    public TileBase TileHostelThree;

    public GameObject MiniGameButton;
    public GameObject ActiveAbilityButton;
    public GameObject AddTeacherButton;
    public GameObject BuildingWindow;
    public GameObject TeacherWindow;
    public GameObject PassiveAbilityDescription;
    public GameObject ActiveAbilityWindow;
    public GameObject JobOffer;
    public GameObject ErrorWindow;
    public AudioClip TapSound;

    // Sprite which will be set after upgrade
    public Sprite done;

    public string BuildingName;
    public int BuildingLevel;

    public int[] MoneyCost;
    public int[] StudentsNeed;

    //Teacher which active at the moment
    private Teacher ActiveTeacher;

    Animator animator;

    public Buildings(GameObject Window)
    {
        this.BuildingWindow = Window;
    }

    private Text _StudentsNeedText;
    private Text _MoneyCostText;
    private Text _BuyButtonText;
    private Text _LevelText;
    private Text _TeacherNameText;
    private Text _StudentsBonusText;
    private Text _RatingBonusText;
    private Text _WageText;
    private Text _TeacherDescriptionText;

    private Image _BuyButtonImage;
    private Click_On_Buildings _LinkOnClickOnBuildings;
    private MainParameters _LinkOnMainParameters;


    void Start()
    {
        animator = GetComponent<Animator>();

        // Find All GameObjects
        if (BuildingWindow.name != "Hostel")
        {
            _StudentsNeedText = BuildingWindow.transform.Find("StudentsNeedIcon/StudentsNeedText").gameObject.GetComponentInChildren<Text>();
            _StudentsNeedText.text = StudentsNeed[BuildingLevel].ToString();
        }
        _MoneyCostText = BuildingWindow.transform.Find("MoneyCostIcon/MoneyCostText").gameObject.GetComponentInChildren<Text>();
        _BuyButtonText = GameObject.Find("BuyButton/BuyButtonText").GetComponent<Text>();
        _LevelText = GameObject.Find("Level/LevelText").GetComponent<Text>();
        _BuyButtonImage = GameObject.Find("BuyButton").GetComponent<Image>();
        _LinkOnClickOnBuildings = GameObject.Find("Buildings").GetComponent<Click_On_Buildings>();
        _LinkOnMainParameters = GameObject.Find("MainParameters").GetComponent<MainParameters>();
        //For teacher
        if (BuildingWindow.name != "Hostel")
        { 
            _TeacherNameText = BuildingWindow.transform.Find("AddTeacherWindow/TeacherNameHeading/TeacherNameText").GetComponent<Text>();
            _StudentsBonusText = BuildingWindow.transform.Find("AddTeacherWindow/StudentsBonusIcon/StudentsBonusText").GetComponent<Text>();
            _RatingBonusText = BuildingWindow.transform.Find("AddTeacherWindow/RatingBonusIcon/RatingBonusText").GetComponent<Text>();
            _WageText = BuildingWindow.transform.Find("AddTeacherWindow/WageIcon/WageText").GetComponent<Text>();
            _TeacherDescriptionText = BuildingWindow.transform.Find("AddTeacherWindow/TeacherDescription/TeacherDescriptionText").GetComponent<Text>();
        }
        // End Block

        _MoneyCostText.text = MoneyCost[BuildingLevel].ToString();

        UpdateMainGameAfterLoad();
    }

    public void Awaking()
    {
        PlayAudio();
        ShowGameWindow(BuildingWindow);
    }

    public void UpgradeAndBuy()
    {
        PlayAudio();

        if (_BuyButtonText.text == "MAX") 
        {
            StartCoroutine(ShowErrorWindow(ErrorWindow, "Нельзя купить. Максимальное улучшение"));
            return;
        }
        else if (CountMoney < MoneyCost[BuildingLevel])
        {
            StartCoroutine(ShowErrorWindow(ErrorWindow, "Нельзя купить. Не хватает денег"));
            return;
        }
        else if (Students < StudentsNeed[BuildingLevel])
        {
            StartCoroutine(ShowErrorWindow(ErrorWindow, "Нельзя купить. Не хватает студентов"));
            return;
        }
        else
        {
            CountMoney -= MoneyCost[BuildingLevel];
            BuildingLevel++;
            MoneyClickBonus *= 1.15f;

            if (BuildingName == "Hostel")
            {
                RatingCount += 70 + 15 * BuildingLevel;
                СapacityStudent += 1000 + 100 * BuildingLevel;
            }

            if (BuildingName == "Kikit")
            {
                MoneyDelta += 2500 * BuildingLevel;
                RatingCount += 127 + 55 * BuildingLevel;
                СapacityStudent += 56 + 24 * BuildingLevel;
            }

            if (BuildingName == "Office")
            {
                MoneyDelta += 1500 + 130 * BuildingLevel;
                RatingCount += 175 + 13 * BuildingLevel;
            }

            if (BuildingName == "NII")
            {
                MoneyDelta -= 5000 * BuildingLevel;
                RatingCount += 200  + 59 * BuildingLevel;
            }

            if (BuildingLevel == 1)
            {
                _BuyButtonText.text = "УЛУЧШИТЬ";
                _LinkOnClickOnBuildings.Swap_image_buildings(TileBefore, TileToSet);

                if (BuildingName == "Hostel")
                {
                    MiniGameButton.SetActive(true);
                }
            }
            if (BuildingLevel == 2 && BuildingName != "Hostel")
            {
                AddTeacherButton.SetActive(true);
            }
            if ((BuildingLevel == 2 && BuildingName == "Hostel") || (BuildingLevel == 3 && BuildingName != "Hostel"))
            {
                PassiveAbilityDescription.SetActive(true);
                if (BuildingName == "Hostel")
                {
                    _LinkOnClickOnBuildings.Swap_image_buildings(TileHostelTwo, TileToSet);
                }
            }
            if ((BuildingLevel == 3 && BuildingName == "Hostel") || (BuildingLevel == 4 && BuildingName != "Hostel"))
            {
                ActiveAbilityButton.SetActive(true);
                if (BuildingName == "Hostel")
                {
                    _LinkOnClickOnBuildings.Swap_image_buildings(TileHostelThree, TileToSet);
                }

                PlayerPrefs.SetFloat("ClickMoneyBonus", MoneyClickBonus);
                _BuyButtonText.text = "MAX";
                _BuyButtonImage.sprite = done;
            }

            if(_BuyButtonText.text != "MAX")
            {
                if (BuildingWindow.name != "Hostel")
                    _StudentsNeedText.text = StudentsNeed[BuildingLevel].ToString();
                _MoneyCostText.text = MoneyCost[BuildingLevel].ToString();
            }
            else
            {
                if (BuildingWindow.name != "Hostel")
                    _StudentsNeedText.text = "-";
                _MoneyCostText.text = "-";
            }

            _LevelText.text = "УРОВЕНЬ " + BuildingLevel.ToString();

            _LinkOnMainParameters.UpdateMainParameters();
            Save();
        }
    }

    public void CloseMenu()
    {
        PlayAudio();

        if (ErrorWindow.activeInHierarchy == false)
        {
            Save();
            animator.SetBool("Close_Window", true);
            StartCoroutine(DelayForCloseWindow(BuildingWindow));
        }
    }

    public IEnumerator ShowErrorWindow(GameObject ErrorWindow, string message)
    {
        /*
            Show Error window on 2 seconds

            :param ErrorWindow: Some Window what need show 
            :param message: Text on this Error Window
         */
        ErrorWindow.SetActive(true);
        ErrorWindow.GetComponentInChildren<Text>().text = message;
        yield return new WaitForSeconds(1f);
        ErrorWindow.SetActive(false);
    }

    public void UpdateMainGameAfterLoad()
    {
        if (BuildingName == "Hostel")
            BuildingLevel = PlayerPrefs.GetInt("HostelLevel", BuildingLevel);
        else if (BuildingName == "Kikit")
            BuildingLevel = PlayerPrefs.GetInt("KikitLevel", BuildingLevel);
        else if (BuildingName == "Office")
            BuildingLevel = PlayerPrefs.GetInt("OfficeLevel", BuildingLevel);
        else if (BuildingName == "NII")
            BuildingLevel = PlayerPrefs.GetInt("NIILevel", BuildingLevel);
        
        _LevelText.text = "УРОВЕНЬ " + BuildingLevel.ToString();

        if (BuildingLevel == 0)
        {
            _BuyButtonText.text = "КУПИТЬ";
            _LevelText.text = "НЕ КУПЛЕНО";
        }

        if (BuildingLevel >= 1)
        {
            _BuyButtonText.text = "УЛУЧШИТЬ";
            _LinkOnClickOnBuildings.Swap_image_buildings(TileBefore, TileToSet);

            if (BuildingName == "Hostel")
            {
                MiniGameButton.SetActive(true);
            }
        }

        if (BuildingLevel >= 2 && BuildingName != "Hostel")
            AddTeacherButton.SetActive(true);

        if ((BuildingLevel >= 2 && BuildingName == "Hostel") || BuildingLevel >= 3)
            PassiveAbilityDescription.SetActive(true);

        if ((BuildingLevel == 3 && BuildingName == "Hostel") || BuildingLevel == 4)
        {
            ActiveAbilityButton.SetActive(true);
            _BuyButtonText.text = "MAX";
            if (BuildingWindow.name != "Hostel")
                _StudentsNeedText.text = "-";
            _MoneyCostText.text = "-";
            _BuyButtonImage.sprite = done;
        }

        if(_BuyButtonText.text != "MAX")
        {
            if (BuildingWindow.name != "Hostel")
                _StudentsNeedText.text = StudentsNeed[BuildingLevel].ToString();
            _MoneyCostText.text = MoneyCost[BuildingLevel].ToString();
        }
    }
    
    Teacher GenerateTeacher()
    {
        (string Name, string Surname, string Description) = GenerateTeacherParam();
        int TeacherRatingBonus = Random.Range(30, 75);
        int TeacherStudentsBonus = Random.Range(-5, 10);
        int TeacherWage = -(TeacherRatingBonus * 13 + TeacherStudentsBonus * 32 + 400);
        if(BuildingName == "Kikit")
        {
            TeacherStudentsBonus += 5;
        }
        if (BuildingName == "NII")
        {
            TeacherRatingBonus = TeacherRatingBonus * 2 - 35;
        }
        if (BuildingName == "Office")
        {
            TeacherWage += 300;
        }
        Teacher GeneratedTeacher = new Teacher(Name, Surname, Description, TeacherStudentsBonus, TeacherWage, TeacherRatingBonus);
        return GeneratedTeacher;
    }

    public void ShowTeacher()
    {
        PlayAudio();

        ActiveTeacher = GenerateTeacher();

        TeacherWindow.SetActive(true);

        _TeacherNameText.text = ActiveTeacher.TeacherSurname + " " + ActiveTeacher.TeacherName;

        _StudentsBonusText.text = ActiveTeacher.TeacherStudentsBonus.ToString() + "/мес.";
        SetColorText(_StudentsBonusText, ActiveTeacher.TeacherStudentsBonus);

        _RatingBonusText.text = ActiveTeacher.TeacherRatingBonus.ToString();
        SetColorText(_RatingBonusText, ActiveTeacher.TeacherRatingBonus);

        _WageText.text = ActiveTeacher.TeacherWage.ToString() + "/мес.";
        SetColorText(_WageText, ActiveTeacher.TeacherWage);

        _TeacherDescriptionText.text = ActiveTeacher.TeacherDescription;
    }

    public void AcceptTeacher()
    {
        /*
            Accept teacher, and update Money/Reting Delta
            
            :param PlayerPrefsViewedTeacher: Name In PlayerPrefs - Index Last Viewed Teacher in this Building (Set in Inspector)
        */
        PlayAudio();

        MoneyDelta += ActiveTeacher.TeacherWage;
        RatingCount += ActiveTeacher.TeacherRatingBonus;
        DeltaTeacherStudentsBonus += ActiveTeacher.TeacherStudentsBonus;

        _LinkOnMainParameters.UpdateMainParameters();

        ShowTeacher();
    }

    public void RejectTeacher()
    {
        PlayAudio();

        TeacherWindow.SetActive(false);
        JobOffer.SetActive(false);

        ShowTeacher();
    }

    public void ShowSpecialAbility()
    {
        PlayAudio();

        ActiveAbilityWindow.SetActive(true);
    }

    public void Save()
    {
        if (BuildingName == "Hostel")
            PlayerPrefs.SetInt("HostelLevel", BuildingLevel);
        else if (BuildingName == "Kikit")
            PlayerPrefs.SetInt("KikitLevel", BuildingLevel);
        else if (BuildingName == "Office")
            PlayerPrefs.SetInt("OfficeLevel", BuildingLevel);
        else if (BuildingName == "NII")
            PlayerPrefs.SetInt("NIILevel", BuildingLevel);
    }

    private void SetColorText(Text gameObject, int value)
    {
        if (value > 0)
            gameObject.color = new Color(0, 0.7f, 0);
        else if (value < 0)
            gameObject.color = new Color(0.7f, 0, 0);
        else
            gameObject.color = new Color(0.4f, 0.4f, 0.4f);
    }

    private void PlayAudio()
    {

        BuildingWindow.GetComponent<AudioSource>().pitch = Random.Range(0.8f, 1.2f);
        BuildingWindow.GetComponent<AudioSource>().PlayOneShot(TapSound);

    }
}
