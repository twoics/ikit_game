﻿/* Это скрипт отслеживает нажатия на здания и кнопки меню зданий.
 */
//нет, этот скрипт полная залупа, не дал мне сделать все красиво как я хотел через одно окошко
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class Click_On_Buildings : MonoBehaviour
{
    private Tilemap Map;
    private Camera MainCamera;

    private Vector3 OnClickPosition;
    private Vector3 UpPosition;
    private Vector3Int CellPosition;

    public GameObject OfficeWindow;
    public GameObject KikitWindow;
    public GameObject HostelWindow;
    public GameObject NIIWindow;

    Office office;
    Nii nii;
    Hostel hostel;
    Kikit kikit;
    // Экземпляры классов зданий 

    public TileBase OfficeUnbought;
    public TileBase OfficeBought;
    
    public TileBase NIIUnbought;
    public TileBase NIIBought;

    public TileBase KikitUnbought;
    public TileBase KikitBought;

    public TileBase HostelUnbought;
    public TileBase HostelBought;
    public TileBase HostelTwoLevel;
    public TileBase HostelThreeLevel;

    private MainParameters MainParameters;

    void Start()
    {
        MainParameters = GameObject.Find("MainParameters").GetComponent<MainParameters>();

        office = new Office(OfficeWindow);
        kikit = new Kikit(KikitWindow);
        hostel = new Hostel(HostelWindow);
        nii = new Nii(NIIWindow);

        Map = GetComponent<Tilemap>();
        MainCamera = Camera.main;

        DeserializationFromSave();
    }
    
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
            OnClickPosition = MainCamera.ScreenToWorldPoint(Input.mousePosition);
        if(Input.GetMouseButtonUp(0))
        {
            UpPosition = MainCamera.ScreenToWorldPoint(Input.mousePosition);
            CellPosition = Map.WorldToCell(UpPosition);
            
            if (OnClickPosition == UpPosition) 
            {
                if ((MainParameters.IsMainWindowActive == true) && (CellPosition == new Vector3Int(9, 8, -9) || CellPosition == new Vector3Int(9, 8, -8) || CellPosition == new Vector3Int(9, 9, -9)
                    || CellPosition == new Vector3Int(10, 9, -8) || CellPosition == new Vector3Int(10, 8, -9)))
                    office.Awaking();
                else if ((MainParameters.IsMainWindowActive == true) && (CellPosition == new Vector3Int(12, 12, -8) || CellPosition == new Vector3Int(13, 13, -8) || CellPosition == new Vector3Int(12, 12, -9)
                    || CellPosition == new Vector3Int(10, 9, -8) || CellPosition == new Vector3Int(13, 12, -9)))
                    nii.Awaking();
                else if ((MainParameters.IsMainWindowActive == true) && (CellPosition == new Vector3Int(6, 14, -9) || CellPosition == new Vector3Int(7, 15, -9) || CellPosition == new Vector3Int(7, 15, -8) || CellPosition == new Vector3Int(10, 16, -8)
                    || CellPosition == new Vector3Int(7, 13, -9) || CellPosition == new Vector3Int(8, 14, -9) || CellPosition == new Vector3Int(8, 14, -8) || CellPosition == new Vector3Int(9, 15, -8)))
                    hostel.Awaking();
                else if ((MainParameters.IsMainWindowActive == true) && (CellPosition == new Vector3Int(5, 10, -9) || CellPosition == new Vector3Int(5, 10, -8)))
                    kikit.Awaking();
            }
        }
    }

    public void ClickOnKikit()
    {
        kikit.Awaking();
    }
    public void Swap_image_buildings(TileBase first_tile, TileBase second_tile)
    {
        Map.SwapTile(first_tile, second_tile);
    }

    private void DeserializationFromSave()
    {
        if (PlayerPrefs.GetInt("OfficeLevel") >= 1)
            Swap_image_buildings(OfficeUnbought, OfficeBought);
        if (PlayerPrefs.GetInt("HostelLevel") >= 1)
            Swap_image_buildings(HostelUnbought, HostelBought);
        if (PlayerPrefs.GetInt("HostelLevel") >= 2)
            Swap_image_buildings(HostelTwoLevel, HostelBought);
        if (PlayerPrefs.GetInt("HostelLevel") >= 3)
            Swap_image_buildings(HostelThreeLevel, HostelBought);
        if (PlayerPrefs.GetInt("NIILevel") >= 1)
            Swap_image_buildings(NIIUnbought, NIIBought);
        if (PlayerPrefs.GetInt("KikitLevel") >= 1)
            Swap_image_buildings(KikitUnbought, KikitBought);
    }
}
