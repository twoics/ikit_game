﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class Hostel : Buildings
//апщяга)
{
    public TileBase TileBeforeTwoBuildings;
    public TileBase TileBeforeThreeBuildings;

    // Param for Special Ability
    int ActiveAbilityCost;
    int ActiveAbilityTopBorderCost;
    int CapacityBonus;

    //GameObjects
    private Text _ActivateAbilityCost;
    private Text _AbilityCapacityBonusText;
    private MainParameters _LinkOnMainParam;

    public Hostel(GameObject Window) : base(Window)
    {

    }
    
    void Awake()
    {
        ActiveAbilityCost = PlayerPrefs.GetInt("HostelAbilityMoney", Random.Range(-40000, -75000));
        ActiveAbilityTopBorderCost = PlayerPrefs.GetInt("ActiveAbilityTopBorderCost", -2500);
        CapacityBonus = PlayerPrefs.GetInt("HostelAbilityСapacity", Random.Range(250, 500));

        //Find Game objects
        _ActivateAbilityCost = BuildingWindow.transform.Find("ActiveAbilityWindow/CostIcon/CostText").GetComponent<Text>();
        _AbilityCapacityBonusText = BuildingWindow.transform.Find("ActiveAbilityWindow/CapacityBonusIcon/CapacityBonusText").GetComponent<Text>();
        _LinkOnMainParam = GameObject.Find("MainParameters").GetComponent<MainParameters>();
    }

    public void ActiveAbilityActivate()
    {
        _ActivateAbilityCost.text = ActiveAbilityCost.ToString();
        _AbilityCapacityBonusText.text = CapacityBonus.ToString();
    }

    public void ExpandCapacuty()
    {
        if (CountMoney + ActiveAbilityCost < 0)
            StartCoroutine(ShowErrorWindow(ErrorWindow, "Не хватает денег"));
        else
        {                
            CountMoney += ActiveAbilityCost;
            СapacityStudent += CapacityBonus;

            _LinkOnMainParam.UpdateMainParameters();

            ActiveAbilityCost += Random.Range(-500, ActiveAbilityTopBorderCost);
            CapacityBonus += Random.Range(10, 25);
            ActiveAbilityTopBorderCost -= 1500;

            _ActivateAbilityCost.text = ActiveAbilityCost.ToString();
            _AbilityCapacityBonusText.text = CapacityBonus.ToString();

            PlayerPrefs.SetInt("HostelAbilityMoney", ActiveAbilityCost);
            PlayerPrefs.SetInt("ActiveAbilityTopBorderCost", ActiveAbilityTopBorderCost);
            PlayerPrefs.SetInt("HostelAbilityСapacity", CapacityBonus);
        }
    }

    public void PassiveAbilityActivate()
    {
        if (BuildingLevel == 2)
            CapacityDelta += 25;
    }

    public void StartMiniGame()
    {
        IsMainWindowActive = true;
        PlayerPrefs.Save();
        BuildingWindow.SetActive(false);
        SceneManager.LoadScene("Jumper");
    }
}

