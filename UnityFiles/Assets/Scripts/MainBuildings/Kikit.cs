﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Kikit : Buildings
//кекит гаме
{

    private float RatingBorder;
    
    private Text _MoneyBonusText;
    private Text _RatingNeedText;
    private MainParameters _MainParameters;
    public Kikit(GameObject Window) : base(Window)
    {

    }
    
    void Awake()
    {
        // For Special Ability
        RatingBorder = PlayerPrefs.GetFloat("RatingBorder", 1000);

        _MoneyBonusText = BuildingWindow.transform.Find("ActiveAbilityWindow/MoneyIncrementBonusIcon/MoneyIncrementBonusText").GetComponent<Text>();
        _RatingNeedText = BuildingWindow.transform.Find("ActiveAbilityWindow/RatingNeedIcon/RatingNeedText").GetComponent<Text>();
        _MainParameters = GameObject.Find("MainParameters").GetComponent<MainParameters>();
    }

    public void PassiveAbilityActivate()
    {
        if (BuildingLevel == 3)
        {
            MoneyDelta += 10000;
            StudentsDelta += 70;
            RatingDelta += 15;
        }
    }

    public void ActiveAbilityActivate()
    {
        _MoneyBonusText.text = "x" + IncrementMoneyBonus.ToString();
        _RatingNeedText.text = RatingBorder.ToString();
    }


    public void IncrementIncome()
    {
        if (RatingCount > RatingBorder )
        {
            IncrementMoneyBonus = (float)Math.Round(IncrementMoneyBonus + 0.05f, 2);
            RatingBorder += 1000;
            MoneyDelta += (int)(9 * 0.05 * Students);

            PlayerPrefs.SetFloat("IncrementCount", IncrementMoneyBonus);
            PlayerPrefs.SetFloat("RatingBorder", RatingBorder);

            _MoneyBonusText.text = "x" + IncrementMoneyBonus.ToString();
            _RatingNeedText.text = RatingBorder.ToString();

            _MainParameters.UpdateMainParameters();
        }
        else
            StartCoroutine(ShowErrorWindow(ErrorWindow, "Не хватет рейтинга для улучшения"));
    }
}
