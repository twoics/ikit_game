﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class Nii : Buildings
//Научно-исследовательский институт
{    
    // For Special 
    int MoneyCostFirst;
    int MoneyCostSecond;
    int MoneyCostThird;

    int RatingBonusFirst;
    int RatingBonusSecond;
    int RatingBonusThird;
    
    //Game objects
    private Text _FirstAbilityCostText;
    private Text _SecondAbilityCostText;
    private Text _ThirdAbilityCostText;

    private Text _RatingBonusFirstText;
    private Text _RatingBonusSecondText;
    private Text _RatingBonusThirdText;

    private MainParameters _MainParameters;

    public Nii(GameObject Window) : base(Window)
    {

    }

    void Awake()
    {
        // For Special Ability
        MoneyCostFirst = PlayerPrefs.GetInt("MoneyCostFirst", Random.Range(-50000, -120000));
        MoneyCostSecond = PlayerPrefs.GetInt("MoneyCostSecond", Random.Range(-1000, -2000));
        MoneyCostThird = PlayerPrefs.GetInt("MoneyCostThird", Random.Range(-200000, -500000));

        RatingBonusFirst = PlayerPrefs.GetInt("RatingBonusFirst", Random.Range(50, 150));
        RatingBonusSecond = PlayerPrefs.GetInt("RatingBonusSecond", Random.Range(25, 50));
        RatingBonusThird = PlayerPrefs.GetInt("RatingBonusThird", Random.Range(2500, 5000));

        PlayerPrefs.SetInt("MoneyCostFirst", MoneyCostFirst);
        PlayerPrefs.SetInt("MoneyCostSecond", MoneyCostSecond);
        PlayerPrefs.SetInt("MoneyCostThird", MoneyCostThird);

        PlayerPrefs.SetInt("RatingBonusFirst", RatingBonusFirst);
        PlayerPrefs.SetInt("RatingBonusSecond", RatingBonusSecond);
        PlayerPrefs.SetInt("RatingBonusThird", RatingBonusThird);

        _FirstAbilityCostText = BuildingWindow.transform.Find("ActiveAbilityWindow/FirstUpgrade/MoneyCostFirst").GetComponent<Text>();
        _SecondAbilityCostText = BuildingWindow.transform.Find("ActiveAbilityWindow/SecondUpgrade/MoneyCostSecond").GetComponent<Text>();
        _ThirdAbilityCostText = BuildingWindow.transform.Find("ActiveAbilityWindow/ThirdUpgrade/MoneyCostThird").GetComponent<Text>();

        _RatingBonusFirstText = BuildingWindow.transform.Find("ActiveAbilityWindow/FirstUpgrade/RatingBonusFirst").GetComponent<Text>();
        _RatingBonusSecondText = BuildingWindow.transform.Find("ActiveAbilityWindow/SecondUpgrade/RatingBonusSecond").GetComponent<Text>();
        _RatingBonusThirdText = BuildingWindow.transform.Find("ActiveAbilityWindow/ThirdUpgrade/RatingBonusThird").GetComponent<Text>();

        _MainParameters = GameObject.Find("MainParameters").GetComponent<MainParameters>();
    }

    public void ActiveAbilityActivate()
    {
        _FirstAbilityCostText.text = MoneyCostFirst.ToString() + " $";
        _SecondAbilityCostText.text = MoneyCostSecond.ToString() + " $/мес.";
        _ThirdAbilityCostText.text = MoneyCostThird.ToString() + " $";

        _RatingBonusFirstText.text = RatingBonusFirst.ToString() + " рейт.";
        _RatingBonusSecondText.text = RatingBonusSecond.ToString() + " рейт./мес.";
        _RatingBonusThirdText.text = RatingBonusThird.ToString() + "рейт.";
    }

    public void PassiveAbilityActivate()
    {
        if (BuildingLevel == 3)
        {
            MoneyDelta -= 25000;
            RatingDelta += 50;
        }
    }

    public void BuyUpgrade(int index)
    {
        if(index == 1)
        {
            if (CountMoney + MoneyCostFirst < 0)
                StartCoroutine(ShowErrorWindow(ErrorWindow, "Не хватает денег"));
            else
            {
                CountMoney += MoneyCostFirst;
                int tmp = Random.Range(0, 100);
                if(tmp <= 90)
                {
                    RatingCount += RatingBonusFirst;
                    UpdateCosts(1);
                }
                else
                {
                    StartCoroutine(ShowErrorWindow(ErrorWindow, "Неудачно!"));
                }
            }
        }
        else if(index == 2)
        {
            MoneyDelta += MoneyCostSecond;
            int tmp = Random.Range(0, 100);
            if (tmp <= 55)
            {
                RatingDelta += RatingBonusSecond;
                UpdateCosts(2);
            }
            else
            {
                StartCoroutine(ShowErrorWindow(ErrorWindow, "Неудачно!"));
            }
        }
        else
        {
            if (CountMoney + MoneyCostThird < 0)
                StartCoroutine(ShowErrorWindow(ErrorWindow, "Не хватает денег"));
            else
            {
                CountMoney += MoneyCostThird;
                int tmp = Random.Range(0, 100);
                if (tmp <= 20)
                {
                    RatingCount += RatingBonusThird;
                    UpdateCosts(3);
                }
                else
                {
                    StartCoroutine(ShowErrorWindow(ErrorWindow, "Неудачно!"));
                }
            }
        }

        _MainParameters.UpdateMainParameters();
    }

    public void UpdateCosts(int index)
    {
        if(index == 1)
        {
            MoneyCostFirst += Random.Range(-5000, -12000);
            RatingBonusFirst += Random.Range(5, 10);

            PlayerPrefs.SetInt("MoneyCostFirst", MoneyCostFirst);
            PlayerPrefs.SetInt("RatingBonusFirst", RatingBonusFirst);
        }
        else if(index == 2)
        {
            MoneyCostSecond += Random.Range(-100, -200);
            RatingBonusSecond += Random.Range(0, 10);

            PlayerPrefs.SetInt("MoneyCostSecond", MoneyCostSecond);
            PlayerPrefs.SetInt("RatingBonusSecond", RatingBonusSecond);
        }
        else if(index == 3)
        {
            MoneyCostThird += Random.Range(-20000, -70000);
            RatingBonusThird += Random.Range(250, 400);

            PlayerPrefs.SetInt("MoneyCostThird", MoneyCostThird);
            PlayerPrefs.SetInt("RatingBonusThird", RatingBonusThird);
        }

        ActiveAbilityActivate();
    }
}