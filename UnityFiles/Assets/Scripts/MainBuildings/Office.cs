﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Linq;

public class Office : Buildings
//чтобы до обеда у меня были фотографии человка паука
{
    // Param for Special Ability
    int AdCost;
    int StudentsBonus;
    int RatingBonus;

    //GameObjects
    private Text _AbilityCostText;
    private Text _AbilityStudentsBonusText;
    private Text _AbilityRatingBonusText;

    private MainParameters _MainParameters;
    public Office(GameObject Window) : base(Window)
    {

    }
    
    void Awake()
    {
        // For Special Ability
        AdCost = PlayerPrefs.GetInt("OfficeAbilityCost", Random.Range(-10000, -20000));
        StudentsBonus = PlayerPrefs.GetInt("OfficeAbilityStudentsBonus", Random.Range(50, 150));
        RatingBonus = PlayerPrefs.GetInt("OfficeAbilityRatingBonus", Random.Range(5, 25));

        PlayerPrefs.SetInt("OfficeAbilityCost", AdCost);
        PlayerPrefs.SetInt("OfficeAbilityStudentsBonus", StudentsBonus);
        PlayerPrefs.SetInt("OfficeAbilityRatingBonus", RatingBonus);

        _AbilityCostText = BuildingWindow.transform.Find("ActiveAbilityWindow/CostIcon/CostText").GetComponent<Text>();
        _AbilityStudentsBonusText = BuildingWindow.transform.Find("ActiveAbilityWindow/StudentsBonusIcon/StudentsBonusText").GetComponent<Text>();
        _AbilityRatingBonusText = BuildingWindow.transform.Find("ActiveAbilityWindow/RatingBonusIcon/RatingBonusText").GetComponent<Text>();

        _MainParameters = GameObject.Find("MainParameters").GetComponent<MainParameters>();

    }

    public void PassiveAbilityActivate()
    {
        if(BuildingLevel == 3)
            OfficeBonusStudent = 100;
    }

    public void ActiveAbilityActivate()
    {
        _AbilityCostText.text = AdCost.ToString();
        _AbilityStudentsBonusText.text = StudentsBonus.ToString();
        _AbilityRatingBonusText.text = RatingBonus.ToString();
    }
    
    public void BuyAd()
    {
        AdCost = PlayerPrefs.GetInt("OfficeAbilityCost");
        StudentsBonus = PlayerPrefs.GetInt("OfficeAbilityStudentsBonus");
        RatingBonus = PlayerPrefs.GetInt("OfficeAbilityRatingBonus");
        if (CountMoney + AdCost < 0)
            StartCoroutine(ShowErrorWindow(ErrorWindow, "Не хватает денег для закупки рекламы"));
        else
        {
            CountMoney = CheckParametersUpdate(CountMoney, AdCost, 10000000000);
            Students = (int)CheckParametersUpdate(Students, StudentsBonus, СapacityStudent);
            MoneyDelta += (int)(9 * IncrementBonusMoney * (CheckParametersUpdate(Students, StudentsBonus, СapacityStudent) - Students));
            RatingCount = CheckParametersUpdate(RatingCount, RatingBonus, 10000000000);

            _MainParameters.UpdateMainParameters();

            AdCost += Random.Range(-1512, -4332);
            StudentsBonus += Random.Range(-10, 35);
            RatingBonus += Random.Range(-10, 20);

            PlayerPrefs.SetInt("OfficeAbilityCost", AdCost);
            PlayerPrefs.SetInt("OfficeAbilityStudentsBonus", StudentsBonus);
            PlayerPrefs.SetInt("OfficeAbilityRatingBonus", RatingBonus);

            ActiveAbilityActivate();
        }
    }
}
