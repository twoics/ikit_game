﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;

public class MainParameters : MonoBehaviour
{
    static bool IsActiveMainWindow = true;

    static float MoneyCount;
    static int StudentCapacity;
    static int StudentsCount;
    static float TrustRating;
    static float CountRating;

    static int TeacherStudentsBonusDelta;
    static int DeltaStudentsDecrement;
    static int OfficeStudentBonus;
    static float MoneyIncrementBonus;
    static float ClickMoneyBonus;

    // The value by which money will change every n seconds
    static float DeltaMoney;
    static float DeltaRating;
    static int DeltaStudents;
    static int DeltaCapacity;

    // This param read from txt file, after they use for random generation teachers
    static string[] AllNamesTeachers;
    static string[] AllSurnamesTeachers;
    static string[] AllDescriptionsTeachers;

    public bool IsMainWindowActive
    {
        get { return IsActiveMainWindow; }

        set
        {
            IsActiveMainWindow = value;
        }
    }
    public float CountMoney
    {
        get { return MoneyCount; }

        set
        {
            MoneyCount = value;
        }
    }
    public float IncrementMoneyBonus
    {
        get { return MoneyIncrementBonus; }

        set
        {
            MoneyIncrementBonus = value;
        }
    }
    public float MoneyClickBonus
    {
        get { return ClickMoneyBonus; }

        set
        {
            ClickMoneyBonus = value;
        }
    }
    public float RatingCount
    {
        get { return CountRating; }

        set
        {
            CountRating = value;
        }
    }
    public int CapacityDelta
    {
        get { return DeltaCapacity; }

        set
        {
            DeltaCapacity = value;
        }
    }
    public int СapacityStudent
    {
        get { return StudentCapacity; }

        set
        {
            StudentCapacity = value;
        }
    }
    public int OfficeBonusStudent
    {
        get { return OfficeStudentBonus; }

        set
        {
            OfficeStudentBonus = value;
        }
    }
    public int DeltaTeacherStudentsBonus
    {
        get { return TeacherStudentsBonusDelta; }

        set
        {
            TeacherStudentsBonusDelta = value;
        }
    }
    public int Students
    {
        get { return StudentsCount; }

        set
        {
            StudentsCount = value;
        }
    }
    public float IncrementBonusMoney
    {
        get { return MoneyIncrementBonus; }

        set
        {
            MoneyIncrementBonus = value;
        }
    }
    public float MoneyDelta
    {
        get { return DeltaMoney; }

        set
        {
            DeltaMoney = value;
        }
    }
    public float RatingDelta
    {
        get { return DeltaRating; }

        set
        {
            DeltaRating = value;
        }
    }
    public int StudentsDelta
    {
        get { return DeltaStudents; }

        set
        {
            DeltaStudents = value;
        }
    }
    public float RatingTrust
    {
        get { return TrustRating; }

        set
        {
            if(TrustRating + value <= 100)
                TrustRating = value;
        }
    }

    private ServerManager _serverManager;

    private Text LinkOnMoneyCount;
    private Text LinkOnStudentsText;
    private Text LinkOnRating;
    private Text LinkOnTrustRetingText;

    private Image LinkOnTrustRetingSprite;

    private int CountScoreFromMiniGames;

    void Start()
    {
        // If user doesn't played before -> Set status Played Before
        if(PlayerPrefs.GetInt("IsGameHasBeenStartBefore", 0) != 1)
            PlayerPrefs.SetInt("IsGameHasBeenStartBefore", 1);

        // Read parameters from PlayerPrefs
        MoneyCount = PlayerPrefs.GetFloat("CountMoney", 125000);
        StudentCapacity = PlayerPrefs.GetInt("CapacityStudent", 0);
        StudentsCount = PlayerPrefs.GetInt("StudentsCount", 0);
        CountRating = PlayerPrefs.GetFloat("CountRating", 0);
        TrustRating = PlayerPrefs.GetFloat("TrustRating", 10);

        TeacherStudentsBonusDelta = PlayerPrefs.GetInt("TeacherStudentsBonusDelta", 0);
        DeltaStudentsDecrement = PlayerPrefs.GetInt("DeltaStudentsDecrement", 0);
        MoneyIncrementBonus = PlayerPrefs.GetFloat("MoneyIncrementBonus", 1);
        ClickMoneyBonus = PlayerPrefs.GetFloat("ClickMoneyBonus", 1f);
        OfficeStudentBonus = PlayerPrefs.GetInt("OfficeStudentBonus", 0);

        DeltaMoney = PlayerPrefs.GetFloat("DeltaMoney", 0);
        DeltaRating = PlayerPrefs.GetInt("DeltaRating", 0);
        DeltaStudents = PlayerPrefs.GetInt("DeltaStudents", 0);
        DeltaCapacity = PlayerPrefs.GetInt("DeltaCapacity", 0);

        // Add money and rating from mini games
        CountScoreFromMiniGames = PlayerPrefs.GetInt("CountMoneyFromMiniGames", 0);
        if(CountScoreFromMiniGames != 0)
        {
            CountMoney += CountScoreFromMiniGames * 100 * CountRating / 2500;
            CountRating += CountScoreFromMiniGames / 2;
            PlayerPrefs.SetInt("CountMoneyFromMiniGames", 0);
        }

        // Find Some GameObjects
        LinkOnMoneyCount = GameObject.Find("MoneyText").GetComponent<Text>();
        LinkOnRating = GameObject.Find("RatingText").GetComponent<Text>();
        LinkOnTrustRetingText = GameObject.Find("TrustRatingText").GetComponent<Text>();
        LinkOnStudentsText = GameObject.Find("StudentsText").GetComponent<Text>();

        LinkOnTrustRetingSprite = GameObject.Find("TrustRatingIcon").GetComponent<Image>();

        _serverManager = gameObject.GetComponent<ServerManager>();
        //Read txt from file
        AllNamesTeachers = ((TextAsset)Resources.Load("NamesTeachers", typeof(TextAsset))).text.Split('\n');
        AllSurnamesTeachers = ((TextAsset)Resources.Load("SurnamesTeachers", typeof(TextAsset))).text.Split('\n');
        AllDescriptionsTeachers = ((TextAsset)Resources.Load("DescriptionsTeachers", typeof(TextAsset))).text.Split('\n');

        UpdateMainParameters();
    }

    public void UpdateStats()
    //Функция обновления параметров за каждые 30 секунд, вызывается из класса Date
    {
        MoneyCount = CheckParametersUpdate(MoneyCount, DeltaMoney, 10000000000000);
        CountRating = CheckParametersUpdate(CountRating, DeltaRating, 10000000000000);
        StudentCapacity = (int)CheckParametersUpdate(StudentCapacity, DeltaCapacity, 10000000000000);

        DeltaStudents = (int)(UnityEngine.Random.Range(0, 100) + CountRating / 50 + TeacherStudentsBonusDelta + OfficeStudentBonus - DeltaStudentsDecrement);
        DeltaStudentsDecrement += 2;

        DeltaMoney += 9 * MoneyIncrementBonus * (CheckParametersUpdate(StudentsCount, DeltaStudents, StudentCapacity) - StudentsCount);

        StudentsCount = (int)CheckParametersUpdate(StudentsCount, DeltaStudents, StudentCapacity);

        UpdateMainParameters();
    }

    public void UpdateMainParameters()
    //Updates the value of the parameters on the screen, then set the values in PlayerPrefs
    {
        //Update Rating in DB
        if(_serverManager.IsLogged == true)
            _serverManager.SendLeaderBord((int)RatingCount);

        LinkOnMoneyCount.text = Math.Round(MoneyCount).ToString();
        if (DeltaMoney > 0)
            LinkOnMoneyCount.text += "$ (+";
        else
        {
            LinkOnMoneyCount.text += "$ (";
        }
        LinkOnMoneyCount.text += Math.Round(DeltaMoney).ToString() + "$)";
        LinkOnRating.text = Math.Round(CountRating).ToString();
        LinkOnTrustRetingText.text = ((int)TrustRating).ToString() + "%";
        LinkOnStudentsText.text = StudentsCount.ToString();
        LinkOnStudentsText.text += "/" + StudentCapacity;

        if (TrustRating > 80)
            LinkOnTrustRetingSprite.color = new Color(0f, 1f, 0f);
        else if(TrustRating > 60)
            LinkOnTrustRetingSprite.color = new Color(0.5f, 1f, 0f);
        else if(TrustRating > 40)
            LinkOnTrustRetingSprite.color = new Color(1f, 1f, 0f);
        else if(TrustRating > 20)
            LinkOnTrustRetingSprite.color = new Color(1f, 0.5f, 0f);
        else
            LinkOnTrustRetingSprite.color = new Color(1f, 0f, 0f);

        PlayerPrefs.SetFloat("CountMoney", MoneyCount);
        PlayerPrefs.SetInt("CapacityStudent", StudentCapacity);
        PlayerPrefs.SetInt("StudentsCount", StudentsCount);
        PlayerPrefs.SetFloat("CountRating", CountRating);
        PlayerPrefs.SetFloat("TrustRating", TrustRating);

        PlayerPrefs.SetInt("TeacherStudentsBonusDelta", TeacherStudentsBonusDelta);
        PlayerPrefs.SetInt("DeltaStudentsDecrement", DeltaStudentsDecrement);
        PlayerPrefs.SetFloat("MoneyIncrementBonus", MoneyIncrementBonus);
        PlayerPrefs.SetInt("OfficeStudentBonus", OfficeStudentBonus);

        PlayerPrefs.SetFloat("DeltaMoney", DeltaMoney);
        PlayerPrefs.SetFloat("DeltaRating", DeltaRating);
        PlayerPrefs.SetInt("DeltaStudents", DeltaStudents);
        PlayerPrefs.SetInt("DeltaCapacity", DeltaCapacity);
    }

    public float CheckParametersUpdate(float parameter, float increaser, float border)
    {
        if(parameter + increaser <= 0 || parameter < 0)
        {
            parameter = 0;
        }
        else if(parameter + increaser >= border || parameter > border)
        {
            parameter = border;
        }
        else
        {
            parameter += increaser;
        }
        return parameter;
    }

    public void ShowGameWindow(GameObject Window)
    {
        Window.SetActive(true);
        IsMainWindowActive = false;
    }
    
    public IEnumerator DelayForCloseWindow(GameObject Window, bool IsNeedSetMainWindowActive = true)
    {
        if(IsNeedSetMainWindowActive)
        {
            yield return new WaitForSeconds(0.3f);
            IsMainWindowActive = true;
        }

        yield return new WaitForSeconds(0.8f);
        Window.SetActive(false);
    }

    public (string Name, string Surname, string Description) GenerateTeacherParam()
    {
        string Name = AllNamesTeachers[UnityEngine.Random.Range(0, AllNamesTeachers.Length)];
        string Surname = AllSurnamesTeachers[UnityEngine.Random.Range(0, AllSurnamesTeachers.Length)];
        string Descriprion = AllDescriptionsTeachers[UnityEngine.Random.Range(0, AllDescriptionsTeachers.Length)];

        return (Name, Surname, Descriprion);
    }

    private void Update()
    {
        if(Input.GetKeyDown("space"))
        {
            MoneyCount += 500000000;
            MoneyDelta += 500000000;
            CountRating += 20000;
            StudentCapacity += 100000;
            StudentsCount += 90000;
            UpdateMainParameters();
        }
    }
}
