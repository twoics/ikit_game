﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;

public class Message : MonoBehaviour
//сообщения от сберкота
{
    struct SomeMessage
    {
        public string TypeMessage;
        // Type message can be: "Info", "Choice"
        // Info - Simple info message; can affect on some parameters, without user choice
        // Choice - User can choice - Accept deal, Reject deal

        public string TextMessage;

        public int YMoney;
        public int YMoneyDelta;
        public int YRating;
        public int YStudents;
        public int YStudentsCapacity;
        public int YTrustRating;

        public int NMoney;
        public int NMoneyDelta;
        public int NRating;
        public int NStudents;
        public int NStudentsCapacity;
        public int NTrustRating;

        public SomeMessage(
            string Type, 
            string Text, 
            int YMoney = 0, 
            int YMoneyDelta = 0, 
            int YRating = 0, 
            int YStudents = 0, 
            int YStudentsCapacity = 0, 
            int YTrustRaing = 0,
            int NMoney = 0,
            int NMoneyDelta = 0,
            int NRating = 0,
            int NStudents = 0,
            int NStudentsCapacity = 0,
            int NTrustRaing = 0)
        {
            this.TypeMessage = Type;
            this.TextMessage = Text;
            this.YMoney = YMoney;
            this.YMoneyDelta = YMoneyDelta;
            this.YRating = YRating;
            this.YStudents = YStudents;
            this.YStudentsCapacity = YStudentsCapacity;
            this.YTrustRating = YTrustRaing;
            this.NMoney = NMoney;
            this.NMoneyDelta = NMoneyDelta;
            this.NRating = NRating;
            this.NStudents = NStudents;
            this.NStudentsCapacity = NStudentsCapacity;
            this.NTrustRating = NTrustRaing;
        }
    }

    private int NumberViewedMessage;
    // Index of last Accepted message

    float CoolDownMessage = 0;
    bool IsMessageReceived = false;

    public MainParameters main_Parameters;

    [SerializeField] GameObject MessageWindow;

    [SerializeField] Text HeaderField;
    [SerializeField] Text OrderField;

    [SerializeField] GameObject InfoCanvas;
    [SerializeField] GameObject AcceptButton;
    [SerializeField] GameObject RejectButton;

    [SerializeField] GameObject CloseButton;
    [SerializeField] GameObject ErrorWindow;

    //Sprites of message button
    [SerializeField] Sprite DefaultMessage;
    [SerializeField] Sprite MessageCame;

    [SerializeField] GameObject[] MessageIcons;
    [SerializeField] Text[] MessageBonuses;

    [SerializeField] GameObject[] YMessageIcons;
    [SerializeField] Text[] YMessageBonuses;
    [SerializeField] GameObject[] NMessageIcons;
    [SerializeField] Text[] NMessageBonuses;

    Animator animator;

    private Queue<SomeMessage> MessageQueue = new Queue<SomeMessage>();

    public void SwapMessagePicture(string MessageName = null)
    {
        if (MessageName == "message_came")
        {
            gameObject.GetComponent<Image>().sprite = DefaultMessage;
        }
        else
        {
            gameObject.GetComponent<Image>().sprite = MessageCame;
        }
    }
    
    public void CloseWindow()
    {
        SwapMessagePicture("message_came");

        MessageWindow.GetComponent<Animator>().SetBool("IsCloseButtonPressed", true);
        StartCoroutine(main_Parameters.DelayForCloseWindow(MessageWindow));
    }

    private SomeMessage ConvertParametersToMessage(string[] ArrayString)
    {
        if (ArrayString[0] == "Info")
        {
            SomeMessage NewMessage = new SomeMessage(
                ArrayString[0],
                ArrayString[1],
                Random.Range(Convert.ToInt32(ArrayString[2]), Convert.ToInt32(ArrayString[3])),
                Random.Range(Convert.ToInt32(ArrayString[4]), Convert.ToInt32(ArrayString[5])),
                Random.Range(Convert.ToInt32(ArrayString[6]), Convert.ToInt32(ArrayString[7])),
                Random.Range(Convert.ToInt32(ArrayString[8]), Convert.ToInt32(ArrayString[9])),
                Random.Range(Convert.ToInt32(ArrayString[10]), Convert.ToInt32(ArrayString[11])),
                Random.Range(Convert.ToInt32(ArrayString[12]), Convert.ToInt32(ArrayString[13])));
            return NewMessage;
        }
        else
        {
            SomeMessage NewMessage = new SomeMessage(
                ArrayString[0],
                ArrayString[1],
                Random.Range(Convert.ToInt32(ArrayString[2]), Convert.ToInt32(ArrayString[3])),
                Random.Range(Convert.ToInt32(ArrayString[4]), Convert.ToInt32(ArrayString[5])),
                Random.Range(Convert.ToInt32(ArrayString[6]), Convert.ToInt32(ArrayString[7])),
                Random.Range(Convert.ToInt32(ArrayString[8]), Convert.ToInt32(ArrayString[9])),
                Random.Range(Convert.ToInt32(ArrayString[10]), Convert.ToInt32(ArrayString[11])),
                Random.Range(Convert.ToInt32(ArrayString[12]), Convert.ToInt32(ArrayString[13])),
                Random.Range(Convert.ToInt32(ArrayString[14]), Convert.ToInt32(ArrayString[15])),
                Random.Range(Convert.ToInt32(ArrayString[16]), Convert.ToInt32(ArrayString[17])),
                Random.Range(Convert.ToInt32(ArrayString[18]), Convert.ToInt32(ArrayString[19])),
                Random.Range(Convert.ToInt32(ArrayString[20]), Convert.ToInt32(ArrayString[21])),
                Random.Range(Convert.ToInt32(ArrayString[22]), Convert.ToInt32(ArrayString[23])),
                Random.Range(Convert.ToInt32(ArrayString[24]), Convert.ToInt32(ArrayString[25])));
            return NewMessage;
        }
    }

    public void GenerateSecondaryMessage()
    {
        TextAsset MessagesTxt = (TextAsset)Resources.Load("MessagesForGenration", typeof(TextAsset));
        int MessageIndex = Random.Range(0, 20);
        string[] ArrayString = new string[26];
        int tmpInt = 0;

        foreach (string tmp in MessagesTxt.text.Split('\n'))
        {
            if (tmpInt == MessageIndex)
            {
                ArrayString = tmp.Split(';');
            }
            tmpInt++;
        }

        MessageQueue.Enqueue(ConvertParametersToMessage(ArrayString));
    }

    public void Awaking()
    {
        MessageWindow.SetActive(true);
        main_Parameters.IsMainWindowActive = false;

        // Show message
        ShowMessage();
    }

    public void ShowMessage()
    {
        if (MessageQueue.Count > 0 && CoolDownMessage == 0)
        {

            MessageWindow.SetActive(true);
            main_Parameters.IsMainWindowActive = false;

            SomeMessage TmpMessage = MessageQueue.Peek();

            OrderField.text = TmpMessage.TextMessage;

            CloseButton.SetActive(false);

            if (TmpMessage.TypeMessage == "Info")
            {
                InfoCanvas.SetActive(true);
                RejectButton.SetActive(false);
                AcceptButton.SetActive(false);

                foreach(GameObject Icon in MessageIcons)
                {
                    Icon.SetActive(false);
                }

                HeaderField.text = "ОПОВЕЩЕНИЕ";

                if (TmpMessage.YMoney != 0)
                {
                    MessageIcons[0].SetActive(true);
                    MessageBonuses[0].text = TmpMessage.YMoney.ToString();
                    SetColorText(MessageBonuses[0], TmpMessage.YMoney);
                }
                if (TmpMessage.YMoneyDelta != 0)
                {
                    MessageIcons[1].SetActive(true);
                    MessageBonuses[1].text = TmpMessage.YMoneyDelta.ToString() + "/мес.";
                    SetColorText(MessageBonuses[1], TmpMessage.YMoneyDelta);
                }
                if (TmpMessage.YRating != 0)
                {
                    MessageIcons[2].SetActive(true);
                    MessageBonuses[2].text = TmpMessage.YRating.ToString();
                    SetColorText(MessageBonuses[2], TmpMessage.YRating);
                }
                if (TmpMessage.YStudents != 0)
                {
                    MessageIcons[3].SetActive(true);
                    MessageBonuses[3].text = TmpMessage.YStudents.ToString();
                    SetColorText(MessageBonuses[3], TmpMessage.YStudents);
                }
                if (TmpMessage.YStudentsCapacity != 0)
                {
                    MessageIcons[4].SetActive(true);
                    MessageBonuses[4].text = TmpMessage.YStudentsCapacity.ToString() + " макс.";
                    SetColorText(MessageBonuses[4], TmpMessage.YStudentsCapacity);
                }
                if (TmpMessage.YTrustRating != 0)
                {
                    MessageIcons[5].SetActive(true);
                    MessageBonuses[5].text = TmpMessage.YTrustRating.ToString();
                    SetColorText(MessageBonuses[5], TmpMessage.YTrustRating);
                }
                AcceptMessage();
            }

            else if (TmpMessage.TypeMessage == "Choice")
            {
                InfoCanvas.SetActive(false);
                RejectButton.SetActive(true);
                AcceptButton.SetActive(true);

                foreach (GameObject Icon in YMessageIcons)
                {
                    Icon.SetActive(false);
                }

                foreach (GameObject Icon in NMessageIcons)
                {
                    Icon.SetActive(false);
                }

                HeaderField.text = "ВЫБОР";

                if (TmpMessage.YMoney != 0)
                {
                    YMessageIcons[0].SetActive(true);
                    YMessageBonuses[0].text = TmpMessage.YMoney.ToString();
                    SetColorText(YMessageBonuses[0], TmpMessage.YMoney);
                }
                if (TmpMessage.YMoneyDelta != 0)
                {
                    YMessageIcons[1].SetActive(true);
                    YMessageBonuses[1].text = TmpMessage.YMoneyDelta.ToString() + "/мес.";
                    SetColorText(YMessageBonuses[1], TmpMessage.YMoneyDelta);
                }
                if (TmpMessage.YRating != 0)
                {
                    YMessageIcons[2].SetActive(true);
                    YMessageBonuses[2].text = TmpMessage.YRating.ToString();
                    SetColorText(YMessageBonuses[2], TmpMessage.YRating);
                }
                if (TmpMessage.YStudents != 0)
                {
                    YMessageIcons[3].SetActive(true);
                    YMessageBonuses[3].text = TmpMessage.YStudents.ToString();
                    SetColorText(YMessageBonuses[3], TmpMessage.YStudents);
                }
                if (TmpMessage.YStudentsCapacity != 0)
                {
                    YMessageIcons[4].SetActive(true);
                    YMessageBonuses[4].text = TmpMessage.YStudentsCapacity.ToString() + " макс.";
                    SetColorText(YMessageBonuses[4], TmpMessage.YStudentsCapacity);
                }
                if (TmpMessage.YTrustRating != 0)
                {
                    YMessageIcons[5].SetActive(true);
                    YMessageBonuses[5].text = TmpMessage.YTrustRating.ToString();
                    SetColorText(YMessageBonuses[5], TmpMessage.YTrustRating);
                }

                if (TmpMessage.NMoney != 0)
                {
                    NMessageIcons[0].SetActive(true);
                    NMessageBonuses[0].text = TmpMessage.NMoney.ToString();
                    SetColorText(NMessageBonuses[0], TmpMessage.NMoney);
                }
                if (TmpMessage.NMoneyDelta != 0)
                {
                    NMessageIcons[1].SetActive(true);
                    NMessageBonuses[1].text = TmpMessage.NMoneyDelta.ToString() + "/мес.";
                    SetColorText(NMessageBonuses[1], TmpMessage.NMoneyDelta);
                }
                if (TmpMessage.NRating != 0)
                {
                    NMessageIcons[2].SetActive(true);
                    NMessageBonuses[2].text = TmpMessage.NRating.ToString();
                    SetColorText(NMessageBonuses[2], TmpMessage.NRating);
                }
                if (TmpMessage.NStudents != 0)
                {
                    NMessageIcons[3].SetActive(true);
                    NMessageBonuses[3].text = TmpMessage.NStudents.ToString();
                    SetColorText(NMessageBonuses[3], TmpMessage.NStudents);
                }
                if (TmpMessage.NStudentsCapacity != 0)
                {
                    NMessageIcons[4].SetActive(true);
                    NMessageBonuses[4].text = TmpMessage.NStudentsCapacity.ToString() + " макс.";
                    SetColorText(NMessageBonuses[4], TmpMessage.NStudentsCapacity);
                }
                if (TmpMessage.NTrustRating != 0)
                {
                    NMessageIcons[5].SetActive(true);
                    NMessageBonuses[5].text = TmpMessage.NTrustRating.ToString();
                    SetColorText(NMessageBonuses[5], TmpMessage.NTrustRating);
                }
            }
        }
    }

    public void AcceptMessage()
    {
        IsMessageReceived = true;
        CoolDownMessage = Random.Range(1f, 10f);

        SomeMessage TmpMessage = MessageQueue.Dequeue();

        // Update parameters after accept message
        main_Parameters.CountMoney = main_Parameters.CheckParametersUpdate(main_Parameters.CountMoney, TmpMessage.YMoney, 10000000000000);
        main_Parameters.MoneyDelta += TmpMessage.YMoneyDelta + (int)(9 * main_Parameters.IncrementBonusMoney * (main_Parameters.CheckParametersUpdate(main_Parameters.Students, TmpMessage.YStudents, main_Parameters.СapacityStudent) - main_Parameters.Students));
        main_Parameters.RatingCount = main_Parameters.CheckParametersUpdate(main_Parameters.RatingCount, TmpMessage.YRating, 10000000000000);
        main_Parameters.СapacityStudent = (int)main_Parameters.CheckParametersUpdate(main_Parameters.СapacityStudent, TmpMessage.YStudentsCapacity, 10000000000000);
        main_Parameters.Students = (int)main_Parameters.CheckParametersUpdate(main_Parameters.Students, TmpMessage.YStudents, main_Parameters.СapacityStudent);
        main_Parameters.RatingTrust = main_Parameters.CheckParametersUpdate(main_Parameters.RatingTrust, TmpMessage.YTrustRating, 100);

        main_Parameters.UpdateMainParameters();

        CloseButton.SetActive(true);

        if(TmpMessage.TypeMessage == "Choice")
        {
            CloseWindow();
        }
    }

    public void RejectMessage()
    {
        IsMessageReceived = true;
        CoolDownMessage = Random.Range(1f, 10f);

        SomeMessage TmpMessage = MessageQueue.Dequeue();

        // Update parameters after accept message
        main_Parameters.CountMoney = main_Parameters.CheckParametersUpdate(main_Parameters.CountMoney, TmpMessage.NMoney, 10000000000000);
        main_Parameters.MoneyDelta += TmpMessage.NMoneyDelta + (int)(9 * main_Parameters.IncrementBonusMoney * (main_Parameters.CheckParametersUpdate(main_Parameters.Students, TmpMessage.NStudents, main_Parameters.СapacityStudent) - main_Parameters.Students));
        main_Parameters.RatingCount = main_Parameters.CheckParametersUpdate(main_Parameters.RatingCount, TmpMessage.NRating, 10000000000000);
        main_Parameters.СapacityStudent = (int)main_Parameters.CheckParametersUpdate(main_Parameters.СapacityStudent, TmpMessage.NStudentsCapacity, 10000000000000);
        main_Parameters.Students = (int)main_Parameters.CheckParametersUpdate(main_Parameters.Students, TmpMessage.NStudents, main_Parameters.СapacityStudent);
        main_Parameters.RatingTrust = main_Parameters.CheckParametersUpdate(main_Parameters.RatingTrust, TmpMessage.NTrustRating, 100);

        main_Parameters.UpdateMainParameters();

        CloseButton.SetActive(true);

        if (TmpMessage.TypeMessage == "Choice")
        {
            CloseWindow();
        }
    }

    public IEnumerator AddMessageAfterDuration(int TimeDuration, int ImpactMoney, int ImpactReting, string TextMessage)
    {
        /*
            Add info message in queue

            :param TimeDuration: Duration before add message
            :param ImpactMoney: Impact on money after read message
            :param ImpactReting: Impact on reting after read message
            :param TextMessage: Text message
        */
        SomeMessage message = new SomeMessage("Info", TextMessage, ImpactMoney, ImpactReting);

        yield return new WaitForSeconds(TimeDuration);

        MessageQueue.Enqueue(message);
    }

    public IEnumerator RandomMessageGenerator()
    {
        yield return new WaitForSeconds(Random.Range(50f, 150f));

        GenerateSecondaryMessage(); 

        StartCoroutine(RandomMessageGenerator());
    }

    private void SetColorText(Text gameObject, int value)
    {
        if (value > 0)
        {
            gameObject.GetComponent<Text>().color = new Color(0, 0.7f, 0);
        }
        else if (value < 0)
        {
            gameObject.GetComponent<Text>().color = new Color(0.7f, 0, 0);
        }
        else
        {
            gameObject.GetComponent<Text>().color = new Color(0.4f, 0.4f, 0.4f);
        }
    }

    private IEnumerator Show_Error_Window(GameObject ErrorWindow, string message)
    {
        /*
            Show Error window on 2 seconds

            :param ErrorWindow: Some Window what need show 
            :param message: Text on this Error Window
         */
        ErrorWindow.SetActive(true);
        ErrorWindow.GetComponentInChildren<Text>().text = message;
        yield return new WaitForSeconds(2f);
        ErrorWindow.SetActive(false);
    }

    private void Start()
    {
        StartCoroutine(RandomMessageGenerator());
        animator = GetComponent<Animator>();
    }
    
    private void Update()
    {
        if(IsMessageReceived == true)
        {
            CoolDownMessage -= Time.deltaTime;
            if (CoolDownMessage <= 0)
            {
                IsMessageReceived = false;

                CoolDownMessage = 0;
            }
        }
        else
        {
            if (MessageQueue.Count > 0)
            {
                SwapMessagePicture();
            }
        }
    }
}
