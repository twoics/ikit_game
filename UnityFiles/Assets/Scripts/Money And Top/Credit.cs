﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Credit : MonoBehaviour
{
    MainParameters mainParameters;

    [SerializeField] GameObject BorrowWindow;
    [SerializeField] GameObject RepayWindow;
    [SerializeField] GameObject ReminderAboutDebt;
    [SerializeField] GameObject ErrorWindow;
    
    [SerializeField] Text BorrowCount;
    [SerializeField] Text RepayCount;
    [SerializeField] Text HowMuchTimeLeft;
    [SerializeField] Text TimeCount;

    int CountBorrow = 10000;

    private bool IsBorrowed;
    int CountRepay;
    float TimeLeft;

    Animator animator;

    void Start()
    {
        mainParameters = GameObject.Find("MainParameters").GetComponent<MainParameters>();
        CountRepay = PlayerPrefs.GetInt("CountRepay", 10000);
        if (PlayerPrefs.GetInt("IsBorrowed", 0) == 1)
        // Пользователь уже брал кредит
        {
            IsBorrowed = true;
        }
        else
        {
            IsBorrowed = false;
        }
        TimeLeft = PlayerPrefs.GetFloat("TimeLeft", 720f);

    }

    public void Awaking()
    {
        if (IsBorrowed == true)
        {
            // Если сейчас займ
            mainParameters.ShowGameWindow(RepayWindow);
            GameObject.Find("CountDebt").GetComponent<Text>().text = CountRepay.ToString();
        }
        else
        {
            // Если сейчас не займ
            TimeCount.text = "720";
            BorrowCount.text = CountBorrow.ToString();
            CountRepay = (int)(CountBorrow * (1.6 - (mainParameters.RatingTrust / 200)));
            RepayCount.text = CountRepay.ToString();
            mainParameters.ShowGameWindow(BorrowWindow);
        }
    }
    
    public void ChangeBorrow(int DeltaBorrow)
    {
        CountBorrow = Convert.ToInt32(BorrowCount.text);
        if(CountBorrow + DeltaBorrow > 0)
        {
            CountBorrow += DeltaBorrow;
            BorrowCount.text = CountBorrow.ToString();
            CountRepay = (int)(CountBorrow * (1.6 - (mainParameters.RatingTrust / 200)));
            RepayCount.text = CountRepay.ToString();
        }
    }
    
    public void OnBorrowPressed()
    {
        mainParameters.CountMoney += CountBorrow;
        mainParameters.UpdateMainParameters();
        IsBorrowed = true;
        BorrowWindow.SetActive(false);
        PlayerPrefs.SetInt("IsBorrowed", 1);
        PlayerPrefs.SetInt("CountRepay", CountRepay);
        PlayerPrefs.SetFloat("TimeLeft", TimeLeft);

        Awaking();
    }

    public void PayOffDebt()
    {
        if(mainParameters.CountMoney < CountRepay)
        {
            StartCoroutine(ShowErrorWindow(ErrorWindow, "Не хвататет средств для выплаты"));
        }
        else
        {
            TimeLeft = 720f;
            IsBorrowed = false;
            mainParameters.CountMoney -= CountRepay;
            GameObject.Find("MainParameters").GetComponent<MainParameters>().UpdateMainParameters();

            CountRepay = 0;
            RepayWindow.SetActive(false);
            
            PlayerPrefs.SetInt("IsBorrowed", 0);
            PlayerPrefs.SetInt("CountRepay", CountRepay);
            PlayerPrefs.SetFloat("TimeLeft", TimeLeft);

            CloseWindow(RepayWindow);
        }
    }
    
    private IEnumerator ShowErrorWindow(GameObject WindowError, string message)
    {
        WindowError.SetActive(true);
        WindowError.GetComponentInChildren<Text>().text = message;
        yield return new WaitForSeconds(1.2f);
        WindowError.SetActive(false);
    }

    public void CloseWindow(GameObject GameWindow)
    {
        if(ErrorWindow.activeInHierarchy == false)
        {
            GameWindow.GetComponent<Animator>().SetTrigger("IsCloseWindow");

            StartCoroutine(mainParameters.DelayForCloseWindow(GameWindow));
        }
    }

    void Update()
    {
        if(IsBorrowed == true)
        {
            TimeLeft -= Time.deltaTime;
            if(RepayWindow.activeInHierarchy == true)
            {
                HowMuchTimeLeft.text = Mathf.Round(TimeLeft).ToString();
                PlayerPrefs.SetFloat("TimeLeft", TimeLeft);

            }
            if (TimeLeft <= 0)
            {
                SceneManager.LoadScene("LoseScene");
            }
        }
    }

}
