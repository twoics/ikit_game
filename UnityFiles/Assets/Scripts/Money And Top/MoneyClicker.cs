﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyClicker : MonoBehaviour
{
    private MainParameters mainParameters;
    private int CountClick;
    private float MoneyClickBonus;

    private float CoolDown;
    [SerializeField] Text CoolDownText;
    [SerializeField] GameObject CoolDownObject;

    void Start()
    {
        mainParameters = GameObject.Find("MainParameters").GetComponent<MainParameters>();
        CountClick = PlayerPrefs.GetInt("CountClick", 0);
        CoolDown = PlayerPrefs.GetFloat("CoolDown", 30f);
    }

    public void OnButtonClick()
    {
        if(CountClick < 60)
        {
            MoneyClickBonus = PlayerPrefs.GetFloat("ClickMoneyBonus", 1f);
            mainParameters.CountMoney += Random.Range(50, 250) * MoneyClickBonus;
            int StudentsBonus = Random.Range(0, 3);
            mainParameters.Students = (int)mainParameters.CheckParametersUpdate(mainParameters.Students, StudentsBonus, mainParameters.СapacityStudent);
            mainParameters.MoneyDelta += (9 * mainParameters.IncrementBonusMoney * (mainParameters.CheckParametersUpdate(mainParameters.Students, StudentsBonus, mainParameters.СapacityStudent) - mainParameters.Students));
            mainParameters.UpdateMainParameters();
            CountClick++;
            PlayerPrefs.SetInt("CountClick", CountClick);

        }
    }

    private void Update()
    {
        if (CountClick >= 60)
        {
            CoolDownObject.SetActive(true);
            CoolDown -= Time.deltaTime;
            CoolDownText.text = ((int)CoolDown).ToString();

            PlayerPrefs.SetFloat("CoolDown", CoolDown);
            if (CoolDown <= 0)
            {
                CountClick = 0;
                CoolDown = 30f;
                CoolDownObject.SetActive(false);
            }
        }
    }
}
