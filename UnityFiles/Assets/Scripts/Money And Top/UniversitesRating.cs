﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;

public class UniversitesRating : MonoBehaviour
//ТОП 10 ВУЗОВ СТРАНЫ! ЧЕЛЛЕНДЖ ДАЙ 5 ТОПЫ И ФАКТЫ ЛОВУШКА МАЙНКРАФТ НУБ ПРОТИВ ПРО МАМИКС ПРОТИВ ВЛАДА А4 В ТИКТОК!
{
    public struct University
    {
        public string NameUniversity;
        public float RatingUniversity;

        public University(string UniversityName, float UniversityRating)
        {
            this.NameUniversity = UniversityName;
            this.RatingUniversity = UniversityRating;
        }
    }

    [SerializeField] Text FirstPlaceName;
    [SerializeField] Text SecondPlaceName;
    [SerializeField] Text ThirdPlaceName;
    [Space]
    [SerializeField] Text FirstPlaceRatingCount;
    [SerializeField] Text SecondPlaceRatingCount;
    [SerializeField] Text ThirdPlaceRatingCount;
    [Space]
    [SerializeField] Text AntoherPlaceNumber;
    [SerializeField] Text AntoherPlaceRatingCount;
    [Space]
    [SerializeField] GameObject AntoherPlaceObject;
    [SerializeField] GameObject UniversitiesRatingWindow;
    [Space]
    [SerializeField] List<string> UniversityNames;
    [SerializeField] List<float> UniversityRating;

    MainParameters mainParameters;

    float MonthCoefficient;

    List<University> Universities = new List<University>();

    void Start()
    {
        mainParameters = GameObject.Find("MainParameters").gameObject.GetComponent<MainParameters>();

        MonthCoefficient = PlayerPrefs.GetFloat("MonthCoefficient", 1);

        for (int i = 0; i < UniversityNames.Count; i++)
        {
            Universities.Add(new University(UniversityNames[i], PlayerPrefs.GetFloat(UniversityNames[i], UniversityRating[i])));
        }

        University SIB_FU = new University("СФУ", mainParameters.RatingCount);
        Universities.Add(SIB_FU);
    }
    
    public void Awaking()
    {
        mainParameters.ShowGameWindow(UniversitiesRatingWindow);
        UpdateRatingCountUniversities(true);
        CheckMainUniversityRating();
    }

    public void CloseRatingWindow()
    {
        UniversitiesRatingWindow.GetComponent<Animator>().SetTrigger("IsCloseWindow");
        StartCoroutine(mainParameters.DelayForCloseWindow(UniversitiesRatingWindow));
    }

    public void CheckMainUniversityRating()
    {
        /*
            Sorts and displays university top
        */
        Universities.Sort((p2, p1) => p1.RatingUniversity.CompareTo(p2.RatingUniversity));

        FirstPlaceName.text = Universities[0].NameUniversity;
        FirstPlaceRatingCount.text = Universities[0].RatingUniversity.ToString();

        SecondPlaceName.text = Universities[1].NameUniversity;
        SecondPlaceRatingCount.text = Universities[1].RatingUniversity.ToString();

        ThirdPlaceName.text = Universities[2].NameUniversity;
        ThirdPlaceRatingCount.text = Universities[2].RatingUniversity.ToString();

        // If SFU rating is lower than the last one, show an additional line
        if ((int)mainParameters.RatingCount < Universities[2].RatingUniversity)
        {
            AntoherPlaceObject.SetActive(true);
            AntoherPlaceNumber.text = (FindIndexOfSFUUniversity() + 1).ToString();
            AntoherPlaceRatingCount.text = ((int)mainParameters.RatingCount).ToString();
        }
        else
            AntoherPlaceObject.SetActive(false);
    }
    
    private int FindIndexOfSFUUniversity()
    {
        for(int i = 0; i < Universities.Count; i++)
        {
            if (Universities[i].NameUniversity == "СФУ")
                return i;
        }
        return -1;
    }
    
    public void UpdateRatingCountUniversities(bool IsUpdateOnlySFU)
    {
        // Someday fix this shit (◕‿◕)
        float NewRatingCount;
        
        // This condition is called only when Reting Window opening, that necessary for correct work, becouse Top Reting update only once in "n" seconds, and sfu rating can change in these "n" seconds
        if (IsUpdateOnlySFU)
        {
            NewRatingCount = mainParameters.RatingCount;
            int SFU_Index = FindIndexOfSFUUniversity();
            University TmpUniversity = new University(Universities[SFU_Index].NameUniversity, NewRatingCount);
            Universities.RemoveAt(SFU_Index);
            Universities.Insert(SFU_Index, TmpUniversity);
            return;
        }

        for (int i = 0; i < Universities.Count; i++)
        {
            // If University isn't SFU, means to Rating add Random value
            if (Universities[i].NameUniversity != "СФУ")
                NewRatingCount = Universities[i].RatingUniversity + Random.Range(-200, 350) * (1 + i / 15) * MonthCoefficient;
            // If University is SFU, means Rating = Rating SFU
            else
                NewRatingCount = mainParameters.RatingCount;

            MonthCoefficient += 0.05f;
            PlayerPrefs.SetFloat("MonthCoefficient", MonthCoefficient);

            University TmpUniversity = new University(Universities[i].NameUniversity, NewRatingCount);
            Universities.RemoveAt(i);
            Universities.Insert(i, TmpUniversity);
            SaveData(TmpUniversity);
        }

        CheckMainUniversityRating();
    }

    private void SaveData(University TmpUniversity)
    {
        // Becouse SFU Rating in MainParameters
        if (TmpUniversity.NameUniversity != "СФУ")
            PlayerPrefs.SetFloat(TmpUniversity.NameUniversity, TmpUniversity.RatingUniversity);
    }

    void Update()
    {

    }
}
