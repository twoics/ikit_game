﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Date : MonoBehaviour
{
    [SerializeField] Text DateField;
    [SerializeField] MainParameters mainParameters;
    [SerializeField] UniversitesRating universitesRating;

    private int _Year;
    private int _Month;
    private float _Day;


    void Start()
    {
        _Year = PlayerPrefs.GetInt("Year", 2006);
        _Month = PlayerPrefs.GetInt("Month", 11);
        _Day = PlayerPrefs.GetInt("Day", 4);    

    }

    void Update()
    {
        _Day += Time.deltaTime;
        if ((int)_Day == 31)
        {
            _Day = 1;
            (_Month, _Year) = (_Month + 1 > 12) ? (1,_Year + 1) : (_Month + 1, _Year);
            PlayerPrefs.SetInt("Month", _Month);
            PlayerPrefs.SetInt("Year", _Year);

            mainParameters.UpdateStats();
            universitesRating.UpdateRatingCountUniversities(false);
        }
        PlayerPrefs.SetInt("Day", (int)_Day);
        DateField.text = ((int)_Day).ToString() + "/" + _Month.ToString() + "/" + _Year.ToString();
    }
}
