﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySomeGameObject : MonoBehaviour
{
    public void DestroyThis()
    {
        Destroy(gameObject);
    }
}
