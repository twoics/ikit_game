﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackInMenu : MonoBehaviour
{
    [SerializeField] GameObject BlackWindow;

    public void BackInMenuLoad()
    {
        BlackWindow.SetActive(true);
        StartCoroutine(LoadWelcomeWindow());
    }   
    private IEnumerator LoadWelcomeWindow()
    {
        PlayerPrefs.DeleteAll();
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("Welcome");
    }
}
