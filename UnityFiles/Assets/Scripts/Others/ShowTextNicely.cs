﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowTextNicely : MonoBehaviour
{
    public Text TextField;
    private string TextFromField;


    void Start()
    {
        TextFromField = TextField.text;
        TextField.text = "";
        StartCoroutine(ShowChar());
    }
    
    private IEnumerator ShowChar()
    {
        foreach(char CharInString in TextFromField)
        {
            TextField.text += CharInString;
            yield return new WaitForSeconds(0.04f);
        }
        yield return new WaitForSeconds(2f);
    }
}
