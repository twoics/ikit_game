﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class LoadGame : MonoBehaviour
{
    public GameObject ErrorWindow;
    public GameObject BlackBackground;
    public GameObject Buttons;
    public GameObject Login;

    public InputField iField;

    //Animator animator;

    void Start()
    {
        //animator = GameObject.Find("Buttons").GetComponent<Animator>();
        
        if (PlayerPrefs.GetInt("IsGameHasBeenStartBefore") == 0)
        {
            Login.SetActive(true);
        }
        else
        {
            Buttons.SetActive(true);
        }
    }

    public void ChoiceNewGame()
    {
        //animator.SetTrigger("TakeAwayButtons");
        Login.SetActive(true);
        Buttons.SetActive(false);
    }
    public void ChoiceContinueGame()
    {
        //animator.SetTrigger("TakeAwayButtons");
        StartCoroutine(LoadMainGame(0));
    }


    public IEnumerator LoadMainGame(int IsNewGameStart)
    {
        Buttons.SetActive(false);
        Login.SetActive(false);

        BlackBackground.SetActive(true);
        BlackBackground.GetComponent<Animator>().SetTrigger("ShowBackgroundWindow");

        yield return new WaitForSeconds(1f);
        if(IsNewGameStart == 1)
            SceneManager.LoadSceneAsync("Tutorial");
        else
            SceneManager.LoadSceneAsync("SampleScene");
    } 

    public IEnumerator ShowErrorWindow()
    {
        ErrorWindow.SetActive(true);
        yield return new WaitForSeconds(2f);
        ErrorWindow.SetActive(false);
    }

    public void OnAcceptLoginPressed()
    {
        PlayerPrefs.DeleteAll();

        string DataFromInput = iField.text;
        if (DataFromInput != "" && DataFromInput != " ")
        {
            PlayerPrefs.SetString("UserName", DataFromInput);
            StartCoroutine(LoadMainGame(1));
        }
    }
}
