﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;

public class ServerManager : MonoBehaviour
{
    public Text TextField;
    public bool IsLogged = false;
    [SerializeField] MainParameters mainParameters;
    [SerializeField] GameObject LeaderBoardWindow;
    Animator animator;

    void Start()
    {
        Login();
    }

    private void Login()
    {
        var request = new LoginWithCustomIDRequest
        {
            CustomId = SystemInfo.deviceUniqueIdentifier,
            CreateAccount = true,
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
            {
                GetPlayerProfile = true
            }
        };
        PlayFabClientAPI.LoginWithCustomID(request, OnSuccess, OnError);
    }
    private void OnSuccess(LoginResult result)
    {
        Debug.Log("Successful login");
        string _name = null;
        if (result.InfoResultPayload.PlayerProfile != null)
            _name = result.InfoResultPayload.PlayerProfile.DisplayName;

        // Set username in DB
        var request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = PlayerPrefs.GetString("UserName"),
        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnDisplayNameUpdate, OnError);
    }
    private void OnDisplayNameUpdate(UpdateUserTitleDisplayNameResult result)
    {
        Debug.Log("On display name update");
        SendLeaderBord((int)mainParameters.RatingCount);
        IsLogged = true;
    }
    //-----Methods for login-----


    public void SendLeaderBord(int score)
    {
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate
                {
                    StatisticName = "RatingScore",
                    Value = score
                }
            }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderBordUpdate, OnError);
    }
    private void OnLeaderBordUpdate(UpdatePlayerStatisticsResult result)
    {
        Debug.Log("Successfull leaderbord sent");
    }
    private void GetLeaderBoard()
    {
        var request = new GetLeaderboardRequest
        {
            StatisticName = "RatingScore",
            StartPosition = 0,
            MaxResultsCount = 10
        };
        PlayFabClientAPI.GetLeaderboard(request, OnLeaderBoardGet, OnError);
    }
    private void OnLeaderBoardGet(GetLeaderboardResult result)
    {
        TextField.text = "";
        foreach(var item in result.Leaderboard)
        {
            TextField.text += item.DisplayName + "            " + item.StatValue + '\n';
        }
    }
    //-----Methods for send/get data from DB-----


    public void LeaderBoardOpen()
    {
        // Send user score in DB
        SendLeaderBord((int)mainParameters.RatingCount);

        LeaderBoardWindow.SetActive(true);

        mainParameters.IsMainWindowActive = false;

        //Show Data from DB
        GetLeaderBoard();
    }
    public void CloseLeaderBoard()
    {
        LeaderBoardWindow.GetComponent<Animator>().SetTrigger("IsClose");
        StartCoroutine(mainParameters.DelayForCloseWindow(LeaderBoardWindow, IsNeedSetMainWindowActive : false));

    }

    private void OnError(PlayFabError error)
    {
        Debug.LogError(error.GenerateErrorReport());
    }
}
