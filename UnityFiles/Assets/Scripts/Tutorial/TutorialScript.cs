﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialScript : MonoBehaviour
{
    public List<GameObject> Children;

    int NumberActiveWindow = 0;
    int IndexChild;
    int AllChilds;

    Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
        Children[NumberActiveWindow].SetActive(true);
        IndexChild = 0;
        AllChilds = Children[NumberActiveWindow].gameObject.transform.childCount;
    }

    public void ShowNextPicture()
    {
        if (IndexChild + 1 < AllChilds)
        {
            IndexChild++;
            Children[NumberActiveWindow].gameObject.transform.GetChild(IndexChild).gameObject.SetActive(true);
        }
        else
            ShowNextBlock();
    }

    private void ShowNextBlock()
    {
        IndexChild = 0;
        if ((NumberActiveWindow + 1) < Children.Count)
        {
            Destroy(Children[NumberActiveWindow]);

            NumberActiveWindow++;
            Children[NumberActiveWindow].SetActive(true);

            AllChilds = Children[NumberActiveWindow].gameObject.transform.childCount;
            Children[NumberActiveWindow].gameObject.transform.GetChild(IndexChild).gameObject.SetActive(true);
        }

        else
        {
            SceneManager.LoadScene("SampleScene");
        }
    }
}
