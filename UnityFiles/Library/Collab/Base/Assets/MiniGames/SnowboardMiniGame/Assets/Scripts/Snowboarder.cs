﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Snowboarder : MonoBehaviour
{
    Rigidbody2D rb;

    public bool isGround = false;
    public int money;
    public Text scoreAttemptCounter;
    public Text scoreTotalCounter;
    private float scoreAttempt;
    private float scoreTotal;

    private int speed = 1500; // Скорость сноубордиста
    private int jumpHeight = 1000; // Высота прыжка
    private int rotationSpeed = -5; // Скорость вращения сноубордиста
    private float horizontalInput; // Нажатие на клавиши "влево" или "вправо"
    private int finish = 58; // Финишная черта (X координата)

    private int xStartPosition = -29;
    private int yStartPosition = 30;
    private int zStartPosition = -1;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.rotation = 0f;

        scoreAttempt = 0;
        scoreTotal = 0;
    }

    void FixedUpdate()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            horizontalInput = Input.acceleration.x;
        }
        else
        {
            horizontalInput = Input.GetAxis("Horizontal");
        }

        rb.rotation += horizontalInput * rotationSpeed;

        // Прыжок
        if (Input.GetKeyDown("space") && isGround)
        {
            rb.AddRelativeForce(transform.up * jumpHeight, ForceMode2D.Impulse);
        }

        // Скорость
        if (transform.position.x > finish)
        {
            rb.transform.position = new Vector3(xStartPosition, yStartPosition, zStartPosition);
            rb.rotation = 0;
            scoreTotal += scoreAttempt;
            scoreAttempt = 0;
        }
        else if (isGround == false)
        {
            speed = 1200;
        }

        // Перемещение сноубордиста
        rb.AddForce(transform.right * speed);

        if (rb.transform.position.y - xStartPosition > scoreAttempt)
        {
            scoreAttempt = rb.transform.position.x - xStartPosition;
        }
        scoreAttemptCounter.text = scoreAttempt.ToString("0");
        scoreTotalCounter.text = scoreTotal.ToString("0");
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            isGround = true;
        }

        if ((collision.gameObject.tag == "fire") || (collision.gameObject.tag == "thorns"))
        {
            scoreTotal += scoreAttempt;
            scoreAttempt = 0;
            rb.transform.position = new Vector3(xStartPosition, yStartPosition, zStartPosition);
            rb.rotation = 0;
        }

        if ((collision.gameObject.tag == "coin10") || (collision.gameObject.tag == "coin20") || (collision.gameObject.tag == "coin50") || (collision.gameObject.tag == "coin100"))
        {
            if (collision.gameObject.tag == "coin10")
            {
                scoreAttempt += 10;
            }
            if (collision.gameObject.tag == "coin20")
            {
                scoreAttempt += 20;
            }
            if (collision.gameObject.tag == "coin50")
            {
                scoreAttempt += 50;
            }
            if (collision.gameObject.tag == "coin100")
            {
                scoreAttempt += 100;
            }

            Destroy(collision.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            isGround = false;
        }
    }
}
