Елисеев Максим Никитич; 50; -4800; 230; Стаж:44 Участвует во внеучебной деятельности. Усидчивость.
Рыбакова Евгения Дмитриевна; 70; -3000; 70; Стаж:3 Активно участвует в жизни университета. Энергичность.
Александров Арсений Глебович; 150; -10000; 500; Стаж:34 Активно проводит исследования в научном центре. Порядочность.
Панкратов Иван Романович; 60; -4300; 210; Стаж:4 Незначительно повышает рейтинг. Принципиальность.
Сергеев Кирилл Романович; 200; -7000; 230; Стаж:9 Привлекает новых абитуриентов. Принципиальность.
Крючков Давид Леонидович; 200; -9000; 500; Стаж:25 Пользуется популярностью у студентов. Эрудированность.
Яковлев Михаил Леонидович; 170; -10000; 500; Стаж:36 Активно проводит исследования в научном центре.
Платонов Тихон Артёмович; 50; -3000; 50; Стаж:7 Почти не влияет на продвижение. Энергичность.
Николаева Кристина Сергеевна; 50; -3000; 250; Стаж:2 Почти не влияет на продвижение. Эрудированность.	
Никифорова Екатерина Давидовна; 70; -4400; 240; Стаж:13 Участвует во внеучебной деятельности. Организованность.
Горшкова Кирилл Марсельевич; 180; -7200; 380; Стаж:13 Активно проводит исследования в научном центре. Усидчивость.
Зайцева Егор Владиславович; 80; -8800; 250; Стаж:21 Активно участвует в жизни университета. Организованность.
Никитин Михаил Ярославович; 60; -3100; 60; Стаж:10 Почти не влияет на продвижение. Честность.
Тихонов Мирослав Даниилович; 90; -10000; 500; Стаж:29 Активно проводит исследования в научном центре. Организованность.
Полякова Ариана Владимировна; 80; -9200; 500; Стаж:18 Пользуется авторитетом в научном сообществе. Организованность.
Максимова Марианна Никитична; 150; -6000; 230; Стаж:7 Привлекает новых абитуриентов. Честность.
Прохорова Яна Андреевна; 200; -7000; 450; Стаж:8 Существенно влияет на рейтинг. Принципиальность.
Лапшина Елизавета Елисеевна; 50; -5000; 480; Стаж:13 Существенно влияет на рейтинг. Эрудированность.
Севастьянова Полина Юрьевна; 70; -4200; 230; Стаж:3 Незначительно повышает рейтинг. Усидчивость.
Синицин Святослав Михайлович; 190; -9300; 480; Стаж:21 Участвует во внеучебной деятельности.
Макаров Павел Тимурович; 140; -4600; 220; Стаж:11 Привлекает новых абитуриентов. Энергичность.
Флоров Артём Лукич; 160; -6100; 360; Стаж:12 Пользуется авторитетом в научном сообществе. Честность.
Котов Пётр Гордеевич; 160; -6600; 340; Стаж:20 Существенно влияет на рейтинг. Принципиальность.
Панов Александр Егорович; 200; -7000; 330; Стаж:20 Существенно влияет на рейтинг. Порядочность.
Михайлова Ольга Михайловна; 200; -7100; 340; Стаж:27 Пользуется популярностью у студентов. Энергичность.
Бородина Александра Михайловна; 190; -8000; 230; Стаж:18 Привлекает новых абитуриентов. Усидчивость.
Смирнов Василий Михайлович; 180; -9000; 500; Стаж:16 Существенно влияет на рейтинг. Эрудированность.
Орлова Диана Матвеевна; 200; -8700; 500; Стаж:15 Пользуется популярностью у студентов. Энергичность.
Осипова Алина Платоновна; 50; -4800; 250; Стаж:35 Активно участвует в жизни университета. Порядочность.
Савина Таисия Егоровна; 80; -3500; 80; Стаж:23 Активно участвует в жизни университета. Усидчивость.