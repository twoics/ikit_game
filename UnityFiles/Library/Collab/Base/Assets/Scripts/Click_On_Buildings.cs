﻿/* Это скрипт отслеживает нажатия на здания и кнопки меню зданий.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class Click_On_Buildings : MonoBehaviour
{
    public bool is_main_window_active = true;
    // Переменная состояния главного окна, true - Открыто главное окно игры, false - открыто другое окно. 

    //bool[] buildings_state; Массив состояний зданий 

    bool is_office_buy;
    bool is_nii_buy;
    bool is_svechki_buy;

    private Tilemap map;
    private Camera main_camera;

    public TileBase TileToSet_Office;
    public TileBase TileBefore_Office;
    public TileBase TileToSet_NII;
    public TileBase TileBefore_NII;
    public TileBase TileToSet_Svechki;
    public TileBase TileBefore_Svechki;
    // Тайлы зданий

    [SerializeField] int price_nii;
    [SerializeField] int price_svechki;
    [SerializeField] int price_office;
    // Цены зданий

    public GameObject Office_Window;
    public GameObject NII_Window;
    public GameObject Svechki_Window;
    public GameObject Kikit_Window;
    // Диалоговые окна

    public GameObject Error_Window_Office;
    public GameObject Error_Window_Nii;
    public GameObject Error_Window_Svechki;
    // Всплывающие окна, указывающие на нехватку средств

    private GameObject message_object;

    private GameObject main_parameters;

    void Start()
    {
        main_parameters = GameObject.Find("Main_Parameters");
        message_object = GameObject.Find("Button_Message");
        map = GetComponent<Tilemap>();
        main_camera = Camera.main;

        /*
        buildings_state = new bool[3]{main_parameters.GetComponent<Main_Parameters>().is_office_buy, main_parameters.GetComponent<Main_Parameters>().is_nii_buy,
                    main_parameters.GetComponent<Main_Parameters>().is_svechki_buy};


        is_office_buy = buildings_state[0];
        is_nii_buy = buildings_state[1];
        is_svechki_buy = buildings_state[2];
        */

    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 click_position = main_camera.ScreenToWorldPoint(Input.mousePosition);
            Vector3Int cell_position = map.WorldToCell(click_position);
            
            if((is_main_window_active == true) && (cell_position == new Vector3Int(9,8,-9) || cell_position == new Vector3Int(9, 8, -8) || cell_position == new Vector3Int(9, 9, -9) 
                || cell_position == new Vector3Int(10, 9, -8) || cell_position == new Vector3Int(10, 8, -9)))
            {
                Office_Window.SetActive(true);
                is_main_window_active = false;
            }
            else if ((is_main_window_active == true) && (cell_position == new Vector3Int(12, 12, -8) || cell_position == new Vector3Int(13, 13, -8) || cell_position == new Vector3Int(12, 12, -9) 
                || cell_position == new Vector3Int(10, 9, -8) || cell_position == new Vector3Int(13, 12, -9)))
            {
                NII_Window.SetActive(true);
                is_main_window_active = false;

            }
            else if ((is_main_window_active == true) && (cell_position == new Vector3Int(6, 14, -9) || cell_position == new Vector3Int(7, 15, -9) || cell_position == new Vector3Int(7, 15, -8) || cell_position == new Vector3Int(10, 16, -8)
                || cell_position == new Vector3Int(7, 13, -9) || cell_position == new Vector3Int(8, 14, -9) || cell_position == new Vector3Int(8, 14, -8) || cell_position == new Vector3Int(9, 15, -8)))
            {
                Svechki_Window.SetActive(true);
                is_main_window_active = false;

            }
            else if ((is_main_window_active == true) && (cell_position == new Vector3Int(5, 10, -9) || cell_position == new Vector3Int(5, 10, -8)))
            {
                Kikit_Window.SetActive(true);
                is_main_window_active = false;

            }
            
        }
    }

    // Задержка на один фрейм 
    public IEnumerator CoroutineSapmle ()
    {
        yield return null;
        is_main_window_active = true;

    }

    // Закрывает окно ошибки после 1 секунды
    public IEnumerator Close_Error_Window(GameObject Error_Window)
    {
        Error_Window.SetActive(true);
        yield return new WaitForSeconds(1f);
        Error_Window.SetActive(false);

    }

    // Методы взаимодействия с офисом
    public void buy_office()
    {
        // Если офис не куплен и хватает денег на его покупку.
        if ((main_parameters.GetComponent<Main_Parameters>().is_office_buy == false) && (main_parameters.GetComponent<Main_Parameters>().money_count >= price_office))
        {
            GameObject.Find("Buy_Button_Office").GetComponentInChildren<Text>().text = "Buied!";
            GameObject.Find("Buy_Button_Office").GetComponentInChildren<Text>().color = new Color(0, 200, 0);

            map.SwapTile(TileBefore_Office, TileToSet_Office);
            Office_Window.SetActive(false);
            StartCoroutine(CoroutineSapmle());
            main_parameters.GetComponent<Main_Parameters>().is_office_buy = true;
            main_parameters.GetComponent<Main_Parameters>().money_count -= price_office;

            main_parameters.GetComponent<Main_Parameters>().change_money();

            //Отсылка сообщения
            message_object.GetComponent<Message>().message_queue.Enqueue("Congratulations! You Buied Office!");
        }

        else if (main_parameters.GetComponent<Main_Parameters>().is_office_buy == false && (main_parameters.GetComponent<Main_Parameters>().money_count < price_office))
        {
            StartCoroutine(Close_Error_Window(Error_Window_Office));
        }

    }
    public void close_menu_office()
    {
        Office_Window.SetActive(false);
        StartCoroutine(CoroutineSapmle());

    }

    // Методы взаимодействия с меню НИИ
    public void buy_NII()
    {
        if ((main_parameters.GetComponent<Main_Parameters>().is_nii_buy == false) && (main_parameters.GetComponent<Main_Parameters>().money_count >= price_nii))
        {
            GameObject.Find("Buy_Botton_NII").GetComponentInChildren<Text>().text = "Buied!";
            GameObject.Find("Buy_Botton_NII").GetComponentInChildren<Text>().color = new Color(0, 200, 0);

            map.SwapTile(TileBefore_NII, TileToSet_NII);
            NII_Window.SetActive(false);
            StartCoroutine(CoroutineSapmle());
            main_parameters.GetComponent<Main_Parameters>().is_nii_buy = true;
            main_parameters.GetComponent<Main_Parameters>().money_count -= price_nii;

            main_parameters.GetComponent<Main_Parameters>().change_money();            
            
            //Отсылка сообщения
            message_object.GetComponent<Message>().message_queue.Enqueue("Congratulations! You Buied Nii!");
        }

        else if(main_parameters.GetComponent<Main_Parameters>().is_nii_buy == false && (main_parameters.GetComponent<Main_Parameters>().money_count < price_nii))
        {
            StartCoroutine(Close_Error_Window(Error_Window_Nii));
        }
    }
    public void close_menu_NII()
    {
        NII_Window.SetActive(false);
        StartCoroutine(CoroutineSapmle());

    }

    // Методы взаимодействия с меню Свечек
    public void buy_Svechki()
    {
        if ((main_parameters.GetComponent<Main_Parameters>().is_svechki_buy == false) && (main_parameters.GetComponent<Main_Parameters>().money_count >= price_svechki))
        {
            GameObject.Find("Buy_Botton_Svechki").GetComponentInChildren<Text>().text = "Buied!";
            GameObject.Find("Buy_Botton_Svechki").GetComponentInChildren<Text>().color = new Color(0, 200, 0);

            map.SwapTile(TileBefore_Svechki, TileToSet_Svechki);
            Svechki_Window.SetActive(false);
            StartCoroutine(CoroutineSapmle());
            main_parameters.GetComponent<Main_Parameters>().is_svechki_buy = true;
            main_parameters.GetComponent<Main_Parameters>().money_count -= price_svechki;

            main_parameters.GetComponent<Main_Parameters>().change_money();

            //Отсылка сообщения
            message_object.GetComponent<Message>().message_queue.Enqueue("Congratulations! You Buied Svechki!");

        }

        else if (main_parameters.GetComponent<Main_Parameters>().is_svechki_buy == false && (main_parameters.GetComponent<Main_Parameters>().money_count < price_svechki))
        {
            StartCoroutine(Close_Error_Window(Error_Window_Svechki));
        }
    }
    public void close_menu_Svechki()
    {
        Svechki_Window.SetActive(false);
        StartCoroutine(CoroutineSapmle());
    }

    // Методы взаимодействия с меню Кикита
    public void close_menu_Kikit()
    {
        Kikit_Window.SetActive(false);
        StartCoroutine(CoroutineSapmle());

    }
}
