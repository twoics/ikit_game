﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;
using System.IO;
using System;

public abstract class Buildings : Main_Parameters
{
    public struct Teacher
    {
        public string TeacherName;
        public int ImpactOnStudents;
        public int Salary;
        public int ImpactOnReting;
        public string Description;
        public Teacher(string Name, int StudentsImpact, int TeacherSalary, int RetingImpact, string TeacherDescription)
        {
            /*
            Initialization
                :param Name: Teacher Name
                :param StudentsImpact: Impact on students after accept
                :param TeacherSalary: Salary, by it's amount each month will be deducted from the amount of money
                :param RetingImpact: Impact on reting after accept
                :param TeacherDescription: Description
             */
            this.TeacherName = Name;
            this.ImpactOnStudents = StudentsImpact;
            this.Salary = TeacherSalary;
            this.ImpactOnReting = RetingImpact;
            this.Description = TeacherDescription;
        }
    }

    // Building Tiles in different state
    public TileBase TileBefore;
    public TileBase TileToSet;

    public GameObject BuildingWindow;
    public GameObject ErrorWindow;

    public GameObject AddTeacherButton;
    public GameObject UpdateWindow;

    public GameObject SpecialAbilityWindow;
    public GameObject SpecialAbilityButton;

    public GameObject TeacherMenu;
    public GameObject JobOffer;

    // Sprite which will be set after upgrade
    public Sprite done;

    public bool is_building_buy;
    public int price_building;
    public int building_level;

    public int cost_update1;
    public int cost_update2;
    public int cost_update3;

    public int count_students;
    public int count_students_when_buy;

    public Queue<Teacher> TeachersQueue;
    public int IndexViewedTeacher;

    // Param for teachers
    int money_impact;
    int reting_impact;
    int students_impact;

    Animator animator;

    public Buildings(GameObject Window)
    {
        this.BuildingWindow = Window;

    }

    void Start()
    {
        animator = GetComponent<Animator>();

        if (BuildingWindow.name != "Svechki")
        {
            BuildingWindow.transform.Find("Count_Students").gameObject.GetComponentInChildren<Text>().text = count_students.ToString();

            BuildingWindow.transform.Find("Updates/Third_Update/Cost").gameObject.GetComponent<Text>().text = cost_update3.ToString() + "$"; 
            GameObject.Find("NeedSpaceInSvechki/Text").GetComponent<Text>().text = count_students_when_buy.ToString();
        }
        GameObject.Find("CostBuy/Cost").GetComponent<Text>().text = price_building.ToString() + "$";

        BuildingWindow.transform.Find("Updates/First_Update/Cost").gameObject.GetComponent<Text>().text = cost_update1.ToString() + "$";
        BuildingWindow.transform.Find("Updates/Second_Update/Cost").gameObject.GetComponent<Text>().text = cost_update2.ToString() + "$";

    }

    public void aweakining()
    {

        ShowGameWindow(BuildingWindow);
       
        if (BuildingWindow.name == "Svechki")
        {
            BuildingWindow.transform.Find("Count_Students").gameObject.GetComponentInChildren<Text>().text = AllStudents.ToString() + "/" + СapacitySvechki.ToString();

        }

    }

    public void buy()
    {
        if (BuildingWindow.name != "Svechki")
        {
            if (is_building_buy == true)
            {
                StartCoroutine(Show_Error_Window(ErrorWindow, "Уже куплено"));
                return;
            }
            if (AllStudents + count_students_when_buy > СapacitySvechki)
            {
                StartCoroutine(Show_Error_Window(ErrorWindow, "Нельзя купить. Не хватает мест в общежитии"));
            }
            else
            {
                if (CountMoney >= price_building)
                {
                    count_students += count_students_when_buy;
                    AllStudents += count_students;

                    Destroy(GameObject.Find("NeedSpaceInSvechki"));
                    Destroy(GameObject.Find("CostBuy"));

                    BuildingWindow.transform.Find("Count_Students").gameObject.GetComponentInChildren<Text>().text = count_students.ToString();

                    BuildingWindow.transform.Find("Level/Level_Text").gameObject.GetComponent<Text>().text = building_level.ToString();

                    BuildingWindow.transform.Find("Buy_Button").gameObject.GetComponentInChildren<Text>().text = "Buied!";
                    BuildingWindow.transform.Find("Buy_Button").gameObject.GetComponentInChildren<Text>().color = new Color(0, 200, 0);

                    GameObject.Find("Buildings").GetComponent<Click_On_Buildings>().swap_image_buildings(TileBefore, TileToSet);

                    is_building_buy = true;
                    CountMoney -= price_building;
                    GameObject.Find("Main_Parameters").GetComponent<Main_Parameters>().UpdateMainParameters();

                }
                else if (is_building_buy == false && (CountMoney < price_building))
                {
                    StartCoroutine(Show_Error_Window(ErrorWindow, "Не хватает средств"));
                }
            }
        }

        else
        {
            if (is_building_buy == true)
            {
                StartCoroutine(Show_Error_Window(ErrorWindow, "Уже куплено"));
                return;
            }

            if (CountMoney >= price_building)
            {
                СapacitySvechki = count_students_when_buy;

                Destroy(GameObject.Find("CostBuy"));

                gameObject.transform.Find("Count_Students").gameObject.GetComponentInChildren<Text>().text = AllStudents.ToString() + "/" + СapacitySvechki.ToString();

                gameObject.transform.Find("Buy_Button").gameObject.GetComponentInChildren<Text>().text = "Buied!";
                gameObject.transform.Find("Buy_Button").gameObject.GetComponentInChildren<Text>().color = new Color(0, 200, 0);

                GameObject.Find("Buildings").GetComponent<Click_On_Buildings>().swap_image_buildings(TileBefore, TileToSet);

                is_building_buy = true;
                CountMoney -= price_building;

                GameObject.Find("Main_Parameters").GetComponent<Main_Parameters>().UpdateMainParameters();
            }
            else
            {
                StartCoroutine(Show_Error_Window(ErrorWindow, "Не хватает средств"));
            }
        }

        if(is_building_buy == true)
            UpdateWindow.SetActive(true);

    }

    public void update_buildings()
    {
        if (building_level == 3)
        {
            StartCoroutine(Show_Error_Window(ErrorWindow, "Вы имеете аксимальный уровень прокачки"));
            return;
        }
        if (AllStudents + count_students > СapacitySvechki)
        {
            StartCoroutine(Show_Error_Window(ErrorWindow, "Нельзя купить. Не хватает мест в общежитии"));
        }
        else
        {
            if (is_building_buy == true)
            {
                if (building_level == 0)
                {
                    if (cost_update1 < CountMoney)
                    {
                        building_level++;
                        CountMoney -= cost_update1;

                        AllStudents -= count_students;
                        count_students *= 2;
                        AllStudents += count_students;

                        GameObject.Find("Updates/First_Update").GetComponent<Image>().sprite = done;


                        RetingCount *= (float)1.3;
                        BuildingWindow.transform.Find("Count_Students").gameObject.GetComponentInChildren<Text>().text = count_students.ToString();

                        BuildingWindow.transform.Find("Level/Level_Text").gameObject.GetComponent<Text>().text = building_level.ToString();

                        Destroy(GameObject.Find("Updates/First_Update/Level"));

                        Destroy(GameObject.Find("Updates/First_Update/Cost"));
                    }
                    else
                    {
                        StartCoroutine(Show_Error_Window(ErrorWindow, "Не хватает денег"));
                    }

                }

                else if (building_level == 1)
                {
                    if (cost_update2 < CountMoney)
                    {
                        building_level++;
                        CountMoney -= cost_update2;

                        AddTeacherButton.SetActive(true);

                        AllStudents -= count_students;
                        count_students *= 2;
                        AllStudents += count_students;

                        GameObject.Find("Updates/Second_Update").GetComponent<Image>().sprite = done;

                        RetingCount *= (float)1.2;

                        BuildingWindow.transform.Find("Count_Students").gameObject.GetComponentInChildren<Text>().text = count_students.ToString();

                        BuildingWindow.transform.Find("Level/Level_Text").gameObject.GetComponent<Text>().text = building_level.ToString();

                        Destroy(GameObject.Find("Updates/Second_Update/Level"));

                        Destroy(GameObject.Find("Updates/Second_Update/Cost"));
                    }

                    else
                    {
                        StartCoroutine(Show_Error_Window(ErrorWindow, "Не хватает денег"));
                    }

                }

                else if (building_level == 2)
                {
                    if (cost_update3 < CountMoney)
                    {
                        building_level++;
                        CountMoney -= cost_update3;

                        AllStudents -= count_students;
                        count_students *= 2;
                        AllStudents += count_students;

                        GameObject.Find("Updates/Third_Update").GetComponent<Image>().sprite = done;

                        RetingCount *= (float)1.1;

                        BuildingWindow.transform.Find("Count_Students").gameObject.GetComponentInChildren<Text>().text = count_students.ToString();

                        BuildingWindow.transform.Find("Level/Level_Text").gameObject.GetComponent<Text>().text = building_level.ToString();

                        Destroy(GameObject.Find("Updates/Third_Update/Level"));
                        Destroy(GameObject.Find("Updates/Third_Update/Cost"));

                        SpecialAbilityButton.SetActive(true);

                    }

                    else
                    {
                        StartCoroutine(Show_Error_Window(ErrorWindow, "Не хватает денег"));
                    }
                }
                GameObject.Find("Main_Parameters").GetComponent<Main_Parameters>().UpdateMainParameters();

            }
            else
            {
                StartCoroutine(Show_Error_Window(ErrorWindow, "Сначала необходимо купить"));
            }
        }
    }

    public void close_menu()
    {
        if (ErrorWindow.activeInHierarchy == false)
        {
            animator.SetBool("Close_Window", true);
            StartCoroutine(DelayForCloseWindow(BuildingWindow));
        }
    }

    public IEnumerator Show_Error_Window(GameObject ErrorWindow, string message)
    {
        /*
            Show Error window on 2 seconds

            :param ErrorWindow: Some Window what need show 
            :param message: Text on this Error Window
         */
        ErrorWindow.SetActive(true);
        ErrorWindow.GetComponentInChildren<Text>().text = message;
        yield return new WaitForSeconds(2f);
        ErrorWindow.SetActive(false);
    }

    public void UpdateMainGameAfterLoad()
    {
        if(is_building_buy == true)
        {
            UpdateWindow.SetActive(true);
            
            gameObject.transform.Find("Buy_Button").gameObject.GetComponentInChildren<Text>().text = "Buied!";
            gameObject.transform.Find("Buy_Button").gameObject.GetComponentInChildren<Text>().color = new Color(0, 200, 0);
            gameObject.transform.Find("Level/Level_Text").gameObject.GetComponent<Text>().text = building_level.ToString();

            Destroy(GameObject.Find("NeedSpaceInSvechki"));
            Destroy(GameObject.Find("CostBuy"));

        }

        if (BuildingWindow.name != "Svechki")
        {

            if (1 <= building_level)
            {
                GameObject.Find("Updates/First_Update").GetComponent<Image>().sprite = done;

                Destroy(GameObject.Find("Updates/First_Update/Level"));

                Destroy(GameObject.Find("Updates/First_Update/Cost"));

            }
            if (2 <= building_level)
            {
                AddTeacherButton.SetActive(true);

                GameObject.Find("Updates/Second_Update").GetComponent<Image>().sprite = done;

                Destroy(GameObject.Find("Updates/Second_Update/Level"));

                Destroy(GameObject.Find("Updates/Second_Update/Cost"));

            }
            if (3 <= building_level)
            {
                GameObject.Find("Updates/Third_Update").GetComponent<Image>().sprite = done;

                Destroy(GameObject.Find("Updates/Third_Update/Level"));

                Destroy(GameObject.Find("Updates/Third_Update/Cost"));

                SpecialAbilityButton.SetActive(true);

            }
        }
        else
        {
            BuildingWindow.transform.Find("Level/Level_Text").gameObject.GetComponent<Text>().text = building_level.ToString();

            BuildingWindow.transform.Find("Count_Students").gameObject.GetComponentInChildren<Text>().text = AllStudents.ToString() + "/" + СapacitySvechki.ToString();
            if (1 <= building_level)
            {
                GameObject.Find("Updates/First_Update").GetComponent<Image>().sprite = done;

                Destroy(GameObject.Find("Updates/First_Update/Level"));

                Destroy(GameObject.Find("Updates/First_Update/Cost"));

            }
            if (2 <= building_level)
            {
                GameObject.Find("Updates/Second_Update").GetComponent<Image>().sprite = done;

                Destroy(GameObject.Find("Updates/Second_Update/Level"));

                Destroy(GameObject.Find("Updates/Second_Update/Cost"));

                Destroy(GameObject.Find("Internship_lock"));
                SpecialAbilityButton.SetActive(true);

            }
        }
    }

    public Teacher DeserializationTeacherFromText(string TextString)
    {
        /*
            Makes an instance of the Teacher structure    
            
            :param TextString: String from text file, which contains data about some teacher
            :return Teacher
        */
        string[] array_string = TextString.Split(';');
        string TeacherName = array_string[0];
        int ImpactOnStudents = Convert.ToInt32(array_string[1]);
        int SalaryTeacher = Convert.ToInt32(array_string[2]);
        int ImpactOnReting = Convert.ToInt32(array_string[3]);
        string InfoAboutTeacher = (array_string[4]);
        return new Teacher(TeacherName, ImpactOnStudents, SalaryTeacher, ImpactOnReting, InfoAboutTeacher);
    }
        
    public void ShowTeacher()
    {
        if (is_building_buy == true)
        {
            JobOffer.SetActive(true);

            if (TeachersQueue.Count > 0)
            {
                TeacherMenu.SetActive(true);

                money_impact = TeachersQueue.Peek().Salary;
                reting_impact = TeachersQueue.Peek().ImpactOnReting;
                students_impact = TeachersQueue.Peek().ImpactOnStudents;
                GameObject.Find("TeacherName/Name").GetComponent<Text>().text = TeachersQueue.Peek().TeacherName;

                GameObject.Find("Accept/AcceptEffects/Students/Text").GetComponent<Text>().text = students_impact.ToString();
                SetColorText(GameObject.Find("Accept/AcceptEffects/Students/Text"), students_impact);

                GameObject.Find("Accept/AcceptEffects/Money/Text").GetComponent<Text>().text = money_impact.ToString() + "/per moth";
                SetColorText(GameObject.Find("Accept/AcceptEffects/Money/Text"), money_impact);

                GameObject.Find("Accept/AcceptEffects/Reting/Text").GetComponent<Text>().text = reting_impact.ToString();
                SetColorText(GameObject.Find("Accept/AcceptEffects/Reting/Text"), reting_impact);

                GameObject.Find("InfoAboutTeacher").GetComponent<Text>().text = TeachersQueue.Peek().Description;
            }
        }
        else
        {
            StartCoroutine(Show_Error_Window(ErrorWindow, "Сначала необходимо купить"));
        }
    }

    public void AcceptTeacher(string PlayerPrefsViewedTeacher)
    {
        /*
            Accept teacher, and update Money/Reting Delta
            
            :param PlayerPrefsViewedTeacher: Name In PlayerPrefs - Index Last Viewed Teacher in this Building (Set in Inspector)
        */

        // Increase the index of the viewed teacher by 1
        IndexViewedTeacher++;
        PlayerPrefs.SetInt(PlayerPrefsViewedTeacher, IndexViewedTeacher);

        TeachersQueue.Dequeue();
        MoneyDelta += money_impact;
        RetingCount += reting_impact;
        count_students += students_impact;

        gameObject.transform.Find("Count_Students").gameObject.GetComponentInChildren<Text>().text = count_students.ToString();
        GameObject.Find("Main_Parameters").GetComponent<Main_Parameters>().UpdateMainParameters();



        TeacherMenu.SetActive(false);
    }

    public void RejectTeacher(string PlayerPrefsViewedTeacher)
    {        
        /*
            Reject teacher
            
            :param PlayerPrefsViewedTeacher: Name In PlayerPrefs - Index Last Viewed Teacher in this Building (Set in Inspector)
         */
        TeachersQueue.Dequeue();

        // Increase the index of the viewed teacher by 
        IndexViewedTeacher++;
        PlayerPrefs.SetInt(PlayerPrefsViewedTeacher, IndexViewedTeacher);
        
        TeacherMenu.SetActive(false);
        JobOffer.SetActive(false);
    }

    private void SetColorText(GameObject gameObject, int value)
    {
        if(value > 0 )
        {
            gameObject.GetComponent<Text>().color = new Color(0, 0.7f, 0);
        }
        else if (value < 0)
        {
            gameObject.GetComponent<Text>().color = new Color(0.7f, 0, 0);
        }
        else
        {
            gameObject.GetComponent<Text>().color = new Color(0.4f, 0.4f, 0.4f);
        }
    }
    
    public void ShowSpecialAbility()
    {
        if(building_level == 3)
        {
            SpecialAbilityWindow.SetActive(true);
        }
        else
        {
            StartCoroutine(Show_Error_Window(ErrorWindow, "Нужен 3 уровень"));
        }
    }

    public void ReadTeachersFromFile(string PlayerPrefsIndexViewedTecher, string FileName)
    {
        /*
            Read teachers from txt file and add them in Queue

            :param PlayerPrefsIndexViewedTecher: Name In PlayerPrefs - Index Last Viewed Teacher in this Building (Set in Inspector)
            :param FileName: Name File (• ◡•)
        */

        IndexViewedTeacher = PlayerPrefs.GetInt(PlayerPrefsIndexViewedTecher, 0);
        TeachersQueue = new Queue<Teacher>();
        TextAsset TeachersOfficeTxt = (TextAsset)Resources.Load(FileName, typeof(TextAsset));

        int tmpInt = 0;
        foreach (string tmp in TeachersOfficeTxt.text.Split('\n'))
        {
            if (tmpInt >= IndexViewedTeacher)
            {
                TeachersQueue.Enqueue(DeserializationTeacherFromText(tmp));
            }
            tmpInt++;
        }
    }
}
