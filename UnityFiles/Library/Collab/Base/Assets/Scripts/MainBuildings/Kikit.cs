﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Kikit : Buildings
{
    public Kikit(GameObject Window) : base(Window)
    {

    }
    void Awake()
    {
        //Read Workers from file
        ReadTeachersFromFile("ViewedTeachersKikit", "KikitTeachers");

        // Deserialization NII after load
        DeserializationKikit();
    }
    
    public void StartMiniGame()
    {
        IsMainWindowActive = true;
        PlayerPrefs.Save();
        BuildingWindow.SetActive(false);
        SceneManager.LoadScene("Jumper");
    }

    public void Save()
    {
        PlayerPrefs.SetInt("StudentsCountKikit", count_students);
        PlayerPrefs.SetInt("LevelKikit", building_level);
        if (is_building_buy == true)
        {
            PlayerPrefs.SetInt("IsKikitBuy", 1);
        }
    }

    private void DeserializationKikit()
    {
        if (PlayerPrefs.GetInt("IsKikitBuy") == 1)
            is_building_buy = true;
        else
            is_building_buy = false;

        building_level = PlayerPrefs.GetInt("LevelKikit");
        count_students = PlayerPrefs.GetInt("StudentsCountKikit");

        if (is_building_buy)
        {
            UpdateMainGameAfterLoad();
        }
    }
}
