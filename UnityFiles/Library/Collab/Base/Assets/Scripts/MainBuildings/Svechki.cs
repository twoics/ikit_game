﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class Svechki : Buildings
{
    public TileBase TileBefore_Svechki_first;
    public TileBase TileBefore_Svechki_second;

    Main_Parameters main_Parameters;

    public Svechki(GameObject Window) : base(Window)
    {

    }
    void Awake()
    {
        main_Parameters = GameObject.Find("Main_Parameters").GetComponent<Main_Parameters>();

        if (PlayerPrefs.GetInt("IsSvechkiBuy") == 1)
            is_building_buy = true;
        

        else
            is_building_buy = false;

        building_level = PlayerPrefs.GetInt("LevelSvechki");

        if (is_building_buy)
        {
            UpdateMainGameAfterLoad();

        }
    }
    
    public new void update_buildings()
    {    
        // Overriding the update_buildings method
        if (is_building_buy == true)
        {
            if(building_level == 0)
            {
                if (cost_update1 > main_Parameters.CountMoney)
                {
                    StartCoroutine(Show_Error_Window(ErrorWindow, "Не хватает денег"));

                }
                else
                {
                    building_level++;
                    main_Parameters.CountMoney -= cost_update1;
                    GameObject.Find("Buildings").GetComponent<Click_On_Buildings>().swap_image_buildings(TileBefore_Svechki_first, TileToSet);

                    GameObject.Find("Updates/First_Update").GetComponent<Image>().sprite = done;

                    main_Parameters.RetingCount *= (float)1.3;
                    main_Parameters.СapacitySvechki *= 3;
                    gameObject.transform.Find("Count_Students").gameObject.GetComponentInChildren<Text>().text = main_Parameters.AllStudents.ToString() + "/" + main_Parameters.СapacitySvechki.ToString();

                    gameObject.transform.Find("Level/Level_Text").gameObject.GetComponent<Text>().text = building_level.ToString();

                    Destroy(GameObject.Find("Updates/First_Update/Level"));

                    Destroy(GameObject.Find("Updates/First_Update/Cost"));
                }
            }

            else if (building_level == 1)
            {
                if (cost_update2 > main_Parameters.CountMoney)
                {
                    StartCoroutine(Show_Error_Window(ErrorWindow, "Не хватает денег"));

                }
                else
                {
                    building_level++;
                    main_Parameters.CountMoney -= cost_update2;
                    GameObject.Find("Buildings").GetComponent<Click_On_Buildings>().swap_image_buildings(TileBefore_Svechki_second, TileToSet);

                    main_Parameters.СapacitySvechki *= 3;

                    GameObject.Find("Updates/Second_Update").GetComponent<Image>().sprite = done;

                    main_Parameters.RetingCount *= (float)1.2;

                    gameObject.transform.Find("Count_Students").gameObject.GetComponentInChildren<Text>().text = main_Parameters.AllStudents.ToString() + "/" + main_Parameters.СapacitySvechki.ToString();

                    gameObject.transform.Find("Level/Level_Text").gameObject.GetComponent<Text>().text = building_level.ToString();

                    Destroy(GameObject.Find("Updates/Second_Update/Level"));

                    Destroy(GameObject.Find("Updates/Second_Update/Cost"));

                    SpecialAbilityButton.SetActive(true);
                }
            }
            else if(building_level == 2)
            {
                StartCoroutine(Show_Error_Window(ErrorWindow, "Уже достигнут максимальнй уровень прокачки"));
            }
            main_Parameters.UpdateMainParameters();
        }
        else
        {
            StartCoroutine(Show_Error_Window(ErrorWindow, "Уже куплено"));
        }
    }

    public void Save()
    {
        PlayerPrefs.SetInt("LevelSvechki", building_level);
        if (is_building_buy == true)
        {
            PlayerPrefs.SetInt("IsSvechkiBuy", 1);
        }
    }

    public void StartMiniGame()
    {
        PlayerPrefs.Save();
        close_menu();
        SceneManager.LoadScene("SnowBoarder");
        
    }
}

