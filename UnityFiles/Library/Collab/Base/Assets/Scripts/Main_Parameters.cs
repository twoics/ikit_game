﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;

public class Main_Parameters : MonoBehaviour
/*
    Сlass containing a list of the main parameters, and some methods for them
 */
{
    static bool IsActiveMainWindow = true;

    static float MoneyCount;

    static float CountReting;

    static int СapacitySvechkiCount;

    static int AllStudentsCount;

    // The value by which money will change every n seconds
    static int DelataMoney;
    static int DelataReting;

    static float TrustRating;

    public bool IsMainWindowActive
    {
        get { return IsActiveMainWindow; }

        set
        {
            IsActiveMainWindow = value;
        }
    }

    public float CountMoney
    {
        get { return MoneyCount; }

        set
        {
            MoneyCount = value;
        }
    }

    public float RetingCount
    {
        get { return CountReting; }

        set
        {
            CountReting = value;

        }
    }

    public int СapacitySvechki
    {
        get { return СapacitySvechkiCount; }

        set
        {

            СapacitySvechkiCount = value;

        }
    }

    public int AllStudents
    {
        get { return AllStudentsCount; }

        set
        {
            AllStudentsCount = value;
            TrustRating += Random.Range(1f, 2f);
        }
    }
    
    /*
    public bool IsStartBefore
    {
        get { return IsGameHasBeenStartBefore; }
        set
        {
            Debug.LogError("Этому параметру нельзя придать значение извне");
        }
    }
    */
    public int MoneyDelta
    {
        get { return DelataMoney; }

        set
        {
            DelataMoney = value;
        }
    }
    public int RetingDelta
    {
        get { return DelataReting; }

        set
        {
            DelataReting = value;
        }
    }

    public float RetingTrust
    {
        get { return TrustRating; }

        set
        {
            TrustRating = value;
        }
    }

    private Text LinkOnMoneyCount;
    private Text LinkOnReting;
    private Text LinkOnTrustRetingText;

    private Image LinkOnTrustRetingSprite;

    void Start()
    {
        // If user doesn't played before -> Set status Played Before
        if(PlayerPrefs.GetInt("IsGameHasBeenStartBefore", 0) != 1)
            PlayerPrefs.SetInt("IsGameHasBeenStartBefore", 1);

        // Read parameters from PlayerPrefs
        MoneyCount = PlayerPrefs.GetInt("CountMoney", 20000000);
        CountReting = PlayerPrefs.GetInt("CountReting", 200);
        TrustRating = PlayerPrefs.GetFloat("TrustRating", 10);

        СapacitySvechkiCount = PlayerPrefs.GetInt("CapacitySvechki", 0);
        AllStudentsCount = PlayerPrefs.GetInt("AllStudents", 0);

        DelataMoney = PlayerPrefs.GetInt("DelataMoney", 0);
        DelataReting = PlayerPrefs.GetInt("DelataReting", 0);

        // Find Some GameObjects
        LinkOnMoneyCount = GameObject.Find("Count_Money").GetComponent<Text>();
        LinkOnReting = GameObject.Find("Raiting").GetComponent<Text>();
        LinkOnTrustRetingText = GameObject.Find("TrustRetingText").GetComponent<Text>();

        LinkOnTrustRetingSprite = GameObject.Find("TrustIcon").GetComponent<Image>();

        UpdateMainParameters();
        StartCoroutine(UpdateMoney());
    }
    
    private IEnumerator UpdateMoney()
    {
    /*    
        Adds some value to the amount of money or rating every 10 seconds
    */
        if ((DelataMoney != 0) && (MoneyCount + DelataMoney > 0) && (CountReting + DelataReting > 0))
        {
            MoneyCount += DelataMoney;
            CountReting += RetingDelta;
            UpdateMainParameters();
        }
        yield return new WaitForSeconds(10f);
        StartCoroutine(UpdateMoney());
    }

    public void UpdateMainParameters()
    {
    /*
        Updates the value of the parameters on the screen, then set the values in PlayerPrefs
    */
        LinkOnMoneyCount.text = ((int)MoneyCount).ToString() + "$";
        LinkOnReting.text = ((int)CountReting).ToString();
        LinkOnTrustRetingText.text = ((int)TrustRating).ToString() + "%";

        if(TrustRating > 50)
        {
            LinkOnTrustRetingSprite.color = new Color(0f, 0.6f, 0f);
        }
        else if(TrustRating < 50)
        {
            LinkOnTrustRetingSprite.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
            LinkOnTrustRetingSprite.color = new Color(0.6f, 0f, 0f);
        }
        else
        {
            LinkOnTrustRetingSprite.color = new Color(0.8f, 0.5f, 0f);
        }

        PlayerPrefs.SetInt("CountMoney", (int)MoneyCount);
        PlayerPrefs.SetInt("CountReting", (int)CountReting);
        PlayerPrefs.SetFloat("TrustRating", TrustRating);

        PlayerPrefs.SetInt("CapacitySvechki", СapacitySvechkiCount);
        PlayerPrefs.SetInt("AllStudents", AllStudentsCount);
        
        PlayerPrefs.SetInt("DelataMoney", DelataMoney);
        PlayerPrefs.SetInt("DelataReting", DelataReting);
    }

    public void ShowGameWindow(GameObject Window)
    {
        Window.SetActive(true);
        IsMainWindowActive = false;
    }
    
    public IEnumerator DelayForCloseWindow(GameObject Window)
    {
        yield return new WaitForSeconds(0.3f);
        IsMainWindowActive = true;

        yield return new WaitForSeconds(0.8f);
        Window.SetActive(false);
    }
}
