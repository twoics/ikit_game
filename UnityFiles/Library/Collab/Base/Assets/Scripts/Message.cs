﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Message : MonoBehaviour
{
    [SerializeField] Sprite default_message;
    [SerializeField] Sprite message_came;
    // Спрайты кнопки сообщений

    public GameObject message_window;
    // Ссылка на окно сообщейний

    public Queue<string> message_queue = new Queue<string>();
    // Очередь, содержит сообщения которые будут появляться в окошке приказов

    public GameObject message_menu;
    

    void Start()
    {
        message_menu.SetActive(false);

        Debug.Log(message_queue.Count());
    }

    void Update()
    {
        if (message_queue.Count() > 0)
        {

            swap_message_picture();
        }
    }

    // Методы взаимодействия с кнопкой сообщения
    public void open_message()
    {
        GameObject.Find("Buildings").GetComponent<Click_On_Buildings>().is_main_window_active = false;
        
        message_window.SetActive(true);
        
        if (message_queue.Count() > 0)
        {
            message_menu.SetActive(true);
            GameObject.Find("Text_Order").GetComponent<Text>().text = message_queue.Dequeue();
        }
    }
    public void close_message()
    {
        if (gameObject.GetComponent<Image>().sprite.name == "message_came")
        {
            swap_message_picture("message_came");
        }
        StartCoroutine(one_moment_delay());
    }

    // Задержка на один фрейм
    public IEnumerator one_moment_delay()
    {
        yield return null;
        GameObject.Find("Buildings").GetComponent<Click_On_Buildings>().is_main_window_active = true;
    }
    
    public void swap_message_picture(string message_name = null)
    {
        /*
         Функция меняет спрайт сообщения
         param: message_name - Необязательный параметр, указывает на то, какой спрайт сообщения сейчас установлен.
        */
        if (message_name == "message_came")
        {
            gameObject.GetComponent<Image>().sprite = default_message;
        }
        else
        {
            gameObject.GetComponent<Image>().sprite = message_came;
        }
    }
}
