﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;

public class Message : MonoBehaviour
{
    struct SomeMessage
    {
        public string TypeMessage;
        // Type message can be: "Info", "Choice","Story"
        // Info - Simple info message; can affect on some parameters, without user choice
        // Choice - User can choice - Accept deal, Reject deal
        // Story - User can choice - Accept Reject deal; After accept or reject will be a consequence

        public string TextMessage;


        //For type message is a Choice or Story
        public int ImpactOnMoney;
        public int ImpactOnReting;

        //Only For type message is a Story
        public string ConsequenceText;
        public int TimeDuration;
        
        public SomeMessage(string Type, string Text, int DeltaMoney, int DeltaReting, string Consequence = null, int Duration = 0)
        /*
        Initialization
            :param Type: Type of message
            :param Text: Text messaage
            :param DeltaMoney: Impact on count money after accept
            :param DeltaReting: Impact on count reting after accept
            :param Consequence: The message that will be sent after accepting a message of type "Story"
            :param Duration:  Duration before sent Consequence
         */
        {
            this.TypeMessage = Type;
            this.TextMessage = Text;
            this.ImpactOnMoney = DeltaMoney;
            this.ImpactOnReting = DeltaReting;
            this.ConsequenceText = Consequence;
            this.TimeDuration = Duration;
        }
    }

    private int NumberViewedMessage;
    // Index of last Accepted message

    float CoolDownMessage = 0;
    bool IsMessageReceived = false;

    public Main_Parameters main_Parameters;

    [SerializeField] GameObject MessageWindow;

    [SerializeField] Text InfoField;
    [SerializeField] Text OrderField;
    [SerializeField] Text PlotMessageField;

    [SerializeField] GameObject InfoMenu;
    [SerializeField] GameObject SelectionMenu;
    [SerializeField] GameObject MenuWithEffects;

    [SerializeField] GameObject CloseButton;

    //Sprites of message button
    [SerializeField] Sprite DefaultMessage;
    [SerializeField] Sprite MessageCame;


    Animator animator;

    private Queue<SomeMessage> MessageQueue = new Queue<SomeMessage>();

    public void SwapMessagePicture(string MessageName = null)
    {
        if (MessageName == "message_came")
        {
            gameObject.GetComponent<Image>().sprite = DefaultMessage;
        }
        else
        {
            gameObject.GetComponent<Image>().sprite = MessageCame;
        }
    }
    
    public void CloseWindow()
    {
        SwapMessagePicture("message_came");

        MessageWindow.GetComponent<Animator>().SetBool("IsCloseButtonPressed", true);
        StartCoroutine(main_Parameters.DelayForCloseWindow(MessageWindow));
    }

    private void ReadDataFromFile()
    {
        // Param for message
        string TypeTmp;
        string TextTmp;
        int DeltaMon;
        int DeltaRep;
        string TextEffect;
        int DurationTime;

        TextAsset MessagesTxt = (TextAsset)Resources.Load("Messages", typeof(TextAsset));
        int tmpInt = 0;
        NumberViewedMessage = PlayerPrefs.GetInt("NumberViewedMessage", 0);
        foreach (string tmp in MessagesTxt.text.Split('\n'))
        {
            SomeMessage NewMessage;
            if(tmpInt >= NumberViewedMessage)
            // Read only messages that haven't been read
            {
                string[] ArrayString = tmp.Split(';');

                //As soon as posible refactor this shit

                // Create a message and add it to the queue
                (TypeTmp, TextTmp, DeltaMon, DeltaRep) = (ArrayString[0], ArrayString[1], Convert.ToInt32(ArrayString[2]), Convert.ToInt32(ArrayString[3]));
                if (TypeTmp == "Info" || TypeTmp == "Choice")
                {
                    NewMessage = new SomeMessage(TypeTmp, TextTmp, DeltaMon, DeltaRep);
                    MessageQueue.Enqueue(NewMessage);

                }
                else
                {
                    (TextEffect, DurationTime)
                       = (ArrayString[4], Convert.ToInt32(ArrayString[5]));
                    NewMessage = new SomeMessage(TypeTmp, TextTmp, DeltaMon, DeltaRep, TextEffect, DurationTime);
                    MessageQueue.Enqueue(NewMessage);
                }
            }
            tmpInt++;

        }
    }

    public void Awakining()
    {
        MessageWindow.SetActive(true);
        main_Parameters.IsMainWindowActive = false;

        // Show message
        if (MessageQueue.Count > 0 && CoolDownMessage == 0)
        {
            SomeMessage TmpMessage = MessageQueue.Peek();
            if (TmpMessage.TypeMessage == "Info")
            {
                InfoMenu.SetActive(true);
                InfoField.text = TmpMessage.TextMessage;
                CloseButton.SetActive(false);

            }
            else if(TmpMessage.TypeMessage == "Choice")
            {
                SelectionMenu.SetActive(true);
                OrderField.text = TmpMessage.TextMessage;

                GameObject.Find("AcceptOrder/ImpactOnMoney").GetComponent<Text>().text = TmpMessage.ImpactOnMoney.ToString();
                GameObject.Find("AcceptOrder/ImpactOnReting").GetComponent<Text>().text = TmpMessage.ImpactOnReting.ToString();

                GameObject.Find("RejectOrder/ImpactOnReting").GetComponent<Text>().text = (-30).ToString();
            }
            else
            {
                MenuWithEffects.SetActive(true);
                PlotMessageField.text = TmpMessage.TextMessage;
            }
        }
    }

    public void AcceptMessage()
    {
        IsMessageReceived = true;
        CoolDownMessage = Random.Range(5f, 10f);

        // Increase the number of viewed messages by 1, and write it in PlayerPrefs
        NumberViewedMessage++;
        PlayerPrefs.SetInt("NumberViewedMessage", NumberViewedMessage);

        SomeMessage TmpMessage = MessageQueue.Dequeue();
        
        // Update parameters after accept message
        if(TmpMessage.TypeMessage == "Info")
        {
            main_Parameters.CountMoney += TmpMessage.ImpactOnMoney;
            main_Parameters.RetingCount += TmpMessage.ImpactOnReting;
            GameObject.Find("Main_Parameters").GetComponent<Main_Parameters>().UpdateMainParameters();
            CloseButton.SetActive(true);
            InfoMenu.SetActive(false);
            CloseWindow();
        }
        else if(TmpMessage.TypeMessage == "Choice")
        {
            main_Parameters.CountMoney += TmpMessage.ImpactOnMoney;
            main_Parameters.RetingCount += TmpMessage.ImpactOnReting;
            GameObject.Find("Main_Parameters").GetComponent<Main_Parameters>().UpdateMainParameters();

            SelectionMenu.SetActive(false);
            CloseWindow();
        }
        else
        {   
            StartCoroutine(AddMessageAfterDuration(TmpMessage.TimeDuration, TmpMessage.ImpactOnMoney, TmpMessage.ImpactOnReting, TmpMessage.ConsequenceText));
            MenuWithEffects.SetActive(false);
            CloseWindow();

        }
    }

    public IEnumerator AddMessageAfterDuration(int TimeDuration, int ImpactMoney, int ImpactReting, string TextMessage)
    {    
    /*
        Add info message in queue

        :param TimeDuration: Duration before add message
        :param ImpactMoney: Impact on money after read message
        :param ImpactReting: Impact on reting after read message
        :param TextMessage: Text message
    */
        SomeMessage message = new SomeMessage("Info", TextMessage, ImpactMoney, ImpactReting);

        yield return new WaitForSeconds(TimeDuration);

        MessageQueue.Enqueue(message);
    }

    private void Start()
    {
        ReadDataFromFile();

        animator = GetComponent<Animator>();

    }
    
    private void Update()
    {
        if(IsMessageReceived == true)
        {
            CoolDownMessage -= Time.deltaTime;
            if (CoolDownMessage <= 0)
            {
                IsMessageReceived = false;

                CoolDownMessage = 0;
            }
        }
        else
        {
            if (MessageQueue.Count > 0)
            {
                SwapMessagePicture();
            }
        }
    }
}
