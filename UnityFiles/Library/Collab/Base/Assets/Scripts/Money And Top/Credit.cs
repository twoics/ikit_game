﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Credit : MonoBehaviour
{
    Main_Parameters main_Parameters;

    [SerializeField] GameObject BorrowWindow;
    [SerializeField] GameObject RepayWindow;
    [SerializeField] GameObject ReminderAboutDebt;
    [SerializeField] GameObject ErrorWindow;
    
    [SerializeField] Text BorrowCount;
    [SerializeField] Text RepayCount;
    [SerializeField] Text HowMuchTimeLeft;
    
    int CountBorrow = 50000;

    private bool IsBorrowed;
    int CountRepay;
    float TimeLeft;

    Animator animator;

    void Start()
    {
        main_Parameters = GameObject.Find("Main_Parameters").GetComponent<Main_Parameters>();
        CountRepay = PlayerPrefs.GetInt("CountRepay", 50000);
        if (PlayerPrefs.GetInt("IsBorrowed", 0) == 1)
        // Пользователь уже брал кредит
        {
            IsBorrowed = true;
        }
        else
        {
            IsBorrowed = false;
        }
        TimeLeft = PlayerPrefs.GetFloat("TimeLeft", 365f);

    }

    public void Awakeing()
    {
        if (IsBorrowed == true)
        // Если был займ
        {
            main_Parameters.ShowGameWindow(RepayWindow);
            GameObject.Find("CountDebt").GetComponent<Text>().text = CountRepay.ToString();

        }
        else
        {
            BorrowCount.text = CountBorrow.ToString();
            CountRepay = (int)(CountBorrow * (1 + (1 - (main_Parameters.RetingTrust/100) )) );
            RepayCount.text = CountRepay.ToString();
            main_Parameters.ShowGameWindow(BorrowWindow);

        }
    }
    
    public void OnPlusPressed()
    {
        CountBorrow = Convert.ToInt32(BorrowCount.text);
        CountBorrow += 50000;
        BorrowCount.text = CountBorrow.ToString();
        CountRepay = (int)(CountBorrow * (1 + (1 - (main_Parameters.RetingTrust / 100))));
        RepayCount.text = CountRepay.ToString();

    }
    public void OnMinusPressed()
    {
        CountBorrow = Convert.ToInt32(BorrowCount.text);
        if(CountBorrow - 50000 > 0)
        {
            CountBorrow -= 50000;
            BorrowCount.text = CountBorrow.ToString();
            CountRepay = (int)(CountBorrow * (1 + (1 - (main_Parameters.RetingTrust / 100))));
            RepayCount.text = CountRepay.ToString();

        }
    }
    
    public void OnBorrowPressed()
    {
        main_Parameters.CountMoney += CountBorrow;
        main_Parameters.UpdateMainParameters();
        IsBorrowed = true;
        BorrowWindow.SetActive(false);
        PlayerPrefs.SetInt("IsBorrowed", 1);
        PlayerPrefs.SetInt("CountRepay", CountRepay);
        PlayerPrefs.SetFloat("TimeLeft", TimeLeft);

        Awakeing();
    }

    public void PayOffDebt()
    {
        if(main_Parameters.CountMoney < CountRepay)
        {
            StartCoroutine(ShowErrorWindow(ErrorWindow, "Не хвататет средств для выплаты"));
        }
        else
        {
            TimeLeft = 365f;
            IsBorrowed = false;
            main_Parameters.CountMoney -= CountRepay;
            CountRepay = 0;
            RepayWindow.SetActive(false);
            
            PlayerPrefs.SetInt("IsBorrowed", 0);
            PlayerPrefs.SetInt("CountRepay", CountRepay);
            PlayerPrefs.SetFloat("TimeLeft", TimeLeft);

            CloseWindow(RepayWindow);
        }
    }
    
    private IEnumerator ShowErrorWindow(GameObject WindowError, string message)
    {
        WindowError.SetActive(true);
        WindowError.GetComponentInChildren<Text>().text = message;
        yield return new WaitForSeconds(2f);
        WindowError.SetActive(false);
    }

    public void CloseWindow(GameObject GameWindow)
    {
        if(ErrorWindow.activeInHierarchy == false)
        {
            GameWindow.GetComponent<Animator>().SetTrigger("IsCloseWindow");

            StartCoroutine(main_Parameters.DelayForCloseWindow(GameWindow));
        }
    }

    void Update()
    {
        if(IsBorrowed == true)
        {
            TimeLeft -= Time.deltaTime;
            if(RepayWindow.activeInHierarchy == true)
            {
                HowMuchTimeLeft.text = Mathf.Round(TimeLeft).ToString();
                PlayerPrefs.SetFloat("TimeLeft", TimeLeft);

            }
            if (TimeLeft <= 0)
            {
                SceneManager.LoadScene("LoseScene");
            }
        }
    }

}
