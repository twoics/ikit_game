﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MoveCamera : MonoBehaviour
{
    [SerializeField] float left_limit;
    [SerializeField] float right_limit;
    [SerializeField] float bottom_limit;
    [SerializeField] float up_limit;

    Vector3 click_position = Vector3.zero;
    Vector3 now_position = Vector3.zero;
    Vector3 camera_position = Vector3.zero;

    private Tilemap map;
    private Camera main_camera;

    void Start()
    {
        map = GetComponent<Tilemap>();
        main_camera = Camera.main;

    }

    void Update()
    {
        if (GameObject.Find("Buildings").GetComponent<Click_On_Buildings>().is_main_window_active == true && Input.GetMouseButtonDown(0))
        {
            click_position = Input.mousePosition;
            camera_position = transform.position;

        }
        if (GameObject.Find("Buildings").GetComponent<Click_On_Buildings>().is_main_window_active == true && Input.GetMouseButton(0))
        {
            now_position = Input.mousePosition;
            Vector3 direction = Camera.main.ScreenToWorldPoint(now_position) - Camera.main.ScreenToWorldPoint(click_position);
            direction = direction * -1;
            Vector3 position = camera_position + direction;
            transform.position = position;
        }

        transform.position = new Vector3
            (
            Mathf.Clamp(transform.position.x, left_limit, right_limit),
            Mathf.Clamp(transform.position.y, bottom_limit, up_limit),
            -8
            );
    }
}