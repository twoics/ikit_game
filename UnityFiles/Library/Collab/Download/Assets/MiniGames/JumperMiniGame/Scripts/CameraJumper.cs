using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraJumper : MonoBehaviour
{
    public GameObject player;
    public GameObject cam;
    public float currentAttemptMaximumHeight;

    private int xPosition = 0;
    private float yyStartPosition = -146.0f;
    private int zPosition = -1;

    void Start()
    {
        currentAttemptMaximumHeight = yyStartPosition;
    }
    
    void Update()
    {
        if (player.transform.position.y > currentAttemptMaximumHeight)
        {
            currentAttemptMaximumHeight = player.transform.position.y;
        }

        cam.transform.position = new Vector3(xPosition, currentAttemptMaximumHeight, zPosition);
    }
}
