﻿/* Это скрипт отслеживает нажатия на здания и кнопки меню зданий.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class Click_On_Buildings : Main_Parameters
{
    private Tilemap map;
    private Camera main_camera;

    public GameObject OfficeWindow;
    public GameObject KikitWindow;
    public GameObject SvechkiWindow;
    public GameObject Nii_Window;

    Office office;
    Nii nii;
    Svechki svechki;
    Kikit kikit;
    // Экземпляры классов зданий 

    void Start()
    {
        office = new Office(OfficeWindow);
        nii = new Nii(Nii_Window);
        svechki = new Svechki(SvechkiWindow);
        kikit = new Kikit(KikitWindow);


        map = GetComponent<Tilemap>();
        main_camera = Camera.main;
    }
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 click_position = main_camera.ScreenToWorldPoint(Input.mousePosition);
            Vector3Int cell_position = map.WorldToCell(click_position);
            
            if((IsMainWindowActive == true) && (cell_position == new Vector3Int(9,8,-9) || cell_position == new Vector3Int(9, 8, -8) || cell_position == new Vector3Int(9, 9, -9) 
                || cell_position == new Vector3Int(10, 9, -8) || cell_position == new Vector3Int(10, 8, -9)))
            {
                office.aweakining();
            
            }
            else if ((IsMainWindowActive == true) && (cell_position == new Vector3Int(12, 12, -8) || cell_position == new Vector3Int(13, 13, -8) || cell_position == new Vector3Int(12, 12, -9) 
                || cell_position == new Vector3Int(10, 9, -8) || cell_position == new Vector3Int(13, 12, -9)))
            {
                 nii.aweakining();

            }
            else if ((IsMainWindowActive == true) && (cell_position == new Vector3Int(6, 14, -9) || cell_position == new Vector3Int(7, 15, -9) || cell_position == new Vector3Int(7, 15, -8) || cell_position == new Vector3Int(10, 16, -8)
                || cell_position == new Vector3Int(7, 13, -9) || cell_position == new Vector3Int(8, 14, -9) || cell_position == new Vector3Int(8, 14, -8) || cell_position == new Vector3Int(9, 15, -8)))
            {
                svechki.aweakining();

            }
            else if ((IsMainWindowActive == true) && (cell_position == new Vector3Int(5, 10, -9) || cell_position == new Vector3Int(5, 10, -8)))
            {
                kikit.aweakining();

            }
        }
    }
    
    
    public void swap_image_buildings(TileBase first_tile, TileBase second_tile)
    {
        map.SwapTile(first_tile, second_tile);
    }

}
