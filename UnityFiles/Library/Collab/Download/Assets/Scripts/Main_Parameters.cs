﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;

public class Main_Parameters : MonoBehaviour
{
    static bool is_main_window_active = true;

    static float money_count = 100000;

    static float reting_count = 200;

    static int capacity_svechki = 0;

    static int all_students = 0;

    public bool IsMainWindowActive
    {
        get { return is_main_window_active; }

        set
        {
            is_main_window_active = value;
        }
    }

    public float CountMoney
    {
        get { return money_count; }

        set
        {
            money_count = value;
        }
    }

    public float RetingCount
    {
        get { return reting_count; }

        set
        {
            reting_count = value;
        }
    }

    public int СapacitySvechki
    {
        get { return capacity_svechki; }

        set
        {
            if(gameObject.name == "Svechki")
            {
                capacity_svechki = value;
            }
            else
            {
                Debug.LogError("AllStudentInSvechki может изменятся только в подклассе Svechki");
            }
        }
    }
    
    public int AllStudents
    {
        get { return all_students; }

        set
        {
            all_students = value;
        }
    }


    private GameObject link_on_money_count;
    private GameObject link_on_reting;

    void Start()
    {
        link_on_money_count = GameObject.Find("Count_Money");
        link_on_reting = GameObject.Find("Number_In_Raiting");
        change_money();

    }


    void Update()
    {

    }
    
    public void change_money()
    {
        link_on_money_count.GetComponent<Text>().text = ((int)money_count).ToString() + "$";
        link_on_reting.GetComponent<Text>().text = ((int)reting_count).ToString();
    }
    /*
    public IEnumerator Add_Money()
    {
        money_count *= money_multiplier;
        yield return new WaitForSeconds(5f);
        change_money();
        StartCoroutine(Add_Money());

    }
    */
}
