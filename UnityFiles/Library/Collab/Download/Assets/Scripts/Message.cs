﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Message : MonoBehaviour
{
    [SerializeField] Sprite default_message;
    [SerializeField] Sprite message_came;
    // Спрайты кнопки сообщений

    public Queue<string> message_queue = new Queue<string>();
    // Очередь, содержит сообщения которые будут появляться в окошке приказов
    public Queue<int> impact_on_money = new Queue<int>();
    public Queue<int> impact_on_rating = new Queue<int>();

    public GameObject message_menu;

    private GameObject main_parameters;

    void Start()
    {
        StartCoroutine(new_message_came("Hi nigga", -100, 200));
        main_parameters = GameObject.Find("Main_Parameters");
        message_menu.SetActive(false);
    }

    void Update()
    {
        if (message_queue.Count() > 0)
        {            
            //swap_message_picture();
        }
    }

    public IEnumerator new_message_came(string message_string, int impact_money, int impact_reting)
    {
        yield return new WaitForSeconds(3f);
        message_queue.Enqueue(message_string);
        impact_on_money.Enqueue(impact_money);
        impact_on_rating.Enqueue(impact_reting);
        GameObject.Find("Button_Message/Count_Message").GetComponent<Text>().text = message_queue.Count().ToString();
    }
    /*
    // Методы взаимодействия с кнопкой сообщения
    public void open_message()
    {
        main_parameters.GetComponent<Main_Parameters>().is_main_window_active = false;
        
        if (message_queue.Count() > 0)
        {
            message_menu.SetActive(true);
            GameObject.Find("Text_Order").GetComponent<Text>().text = message_queue.Peek();

            int tmp_money_impact = impact_on_money.Peek();

            int tmp_reting_impact = impact_on_rating.Peek();

            GameObject.Find("Accept/Impact_Reting").GetComponent<Text>().text = tmp_reting_impact.ToString() + " rep";
            GameObject.Find("Accept/Impact_Money").GetComponent<Text>().text = tmp_money_impact.ToString() + "$";
            // Если влияние на рейтинг положительное - зеленый цвет, иначе красный
            if (tmp_reting_impact > 0)
            {
                GameObject.Find("Accept/Impact_Money").GetComponent<Text>().color = new Color(0, 255, 0);
            }
            else
            {
                GameObject.Find("Accept/Impact_Reting").GetComponent<Text>().color = new Color(240, 0, 0);
            }
        }
    }
    public void close_message()
    {
        message_menu.SetActive(false);
        if (gameObject.GetComponent<Image>().sprite.name == "message_came")
        {
            swap_message_picture("message_came");
        }
        StartCoroutine(one_moment_delay());
    }

    // Задержка на один фрейм
    public IEnumerator one_moment_delay()
    {
        yield return null;
        main_parameters.GetComponent<Main_Parameters>().is_main_window_active = true;
    }
    
    public void swap_message_picture(string message_name = null)
    {
        if (message_name == "message_came")
        {
            gameObject.GetComponent<Image>().sprite = default_message;
        }
        else
        {
            gameObject.GetComponent<Image>().sprite = message_came;
        }
    }
    public void accept_message()
    {
        message_queue.Dequeue();
        impact_on_money.Dequeue();
        impact_on_rating.Dequeue();
        GameObject.Find("Button_Message/Count_Message").GetComponent<Text>().text = message_queue.Count().ToString();
        message_menu.SetActive(false);
    }
    */
}
