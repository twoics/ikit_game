﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MoveCamera : MonoBehaviour
{
    public GameObject snowboarder; // Сноубордист
    public bool isMiniGame; // Проверка, находимся в мини игре или нет
    private float positionX; // X координата камеры в мини игре
    private float positionY; // Y координата камеры в мини игре
    private float offsetX = 8.7f; // Смещение камеры относительно сноубордиста по X координате
    private int offsetY = 4; // Смещение камеры относительно сноубордиста по Y координате
    private int positionZ = -1000; // Z координата камеры в мини игре

    [SerializeField] float left_limit;
    [SerializeField] float right_limit;
    [SerializeField] float bottom_limit;
    [SerializeField] float up_limit;

    Vector3 click_position = Vector3.zero;
    Vector3 now_position = Vector3.zero;
    Vector3 camera_position = Vector3.zero;

    private Tilemap map;
    private Camera main_camera;

    public GameObject main_parameters;

    void Start()
    {
        map = GetComponent<Tilemap>();
        main_camera = Camera.main;
        isMiniGame = false;
    }

    void Update()
    {
        if (!isMiniGame) // Поведение камеры в главной игре
        {
            if (Input.GetMouseButtonDown(0))
            {
                click_position = Input.mousePosition;
                camera_position = transform.position;

            }
            if (Input.GetMouseButton(0))
            {
                now_position = Input.mousePosition;
                Vector3 direction = Camera.main.ScreenToWorldPoint(now_position) - Camera.main.ScreenToWorldPoint(click_position);
                direction = direction * -1;
                Vector3 position = camera_position + direction;
                transform.position = position;
            }

            transform.position = new Vector3
                (
                Mathf.Clamp(transform.position.x, left_limit, right_limit),
                Mathf.Clamp(transform.position.y, bottom_limit, up_limit),
                -8
                );
        }

        else // Поведение камеры в мини игре про сноубордиста
        {
            positionX = snowboarder.transform.position.x + offsetX;
            positionY = snowboarder.transform.position.y + offsetY;
            transform.position = new Vector3(-40.7f, 20.4f, positionZ);
        }
    }
}