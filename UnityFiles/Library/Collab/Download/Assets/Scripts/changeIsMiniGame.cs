﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeIsMiniGame : MonoBehaviour
{
    public GameObject SnowboarderCanvas;
    public GameObject SnowboarderMap;
    public GameObject Kikit;
    public GameObject MainCanvas1;
    public GameObject MainCanvas2;
    public GameObject MainCanvas3;

    public void GoToSnowboardMiniGame()
    {
        Kikit.SetActive(false);
        MainCanvas1.SetActive(false);
        MainCanvas2.SetActive(false);
        MainCanvas3.SetActive(false);

        SnowboarderCanvas.SetActive(true);
        SnowboarderMap.SetActive(true);
        
        gameObject.GetComponentInParent<MoveCamera>().isMiniGame = true;
    }

    public void GoToMainGame()
    {
        MainCanvas1.SetActive(true);
        MainCanvas2.SetActive(true);
        MainCanvas3.SetActive(true);

        SnowboarderCanvas.SetActive(false);
        SnowboarderMap.SetActive(false);

        gameObject.GetComponentInParent<MoveCamera>().isMiniGame = false;
    }
}
