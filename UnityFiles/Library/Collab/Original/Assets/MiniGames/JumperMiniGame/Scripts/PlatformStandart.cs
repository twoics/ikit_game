using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformStandart : MonoBehaviour
{
    GameObject Player;

    public GameObject Physic;
    private bool CheckerForPhysics;
    private float MinimalStep = 0.15f;

    private void Start()
    {
        Player = GameObject.Find("Player");
    }

    void Update()
    {
        if (Player.transform.position.y > transform.position.y + MinimalStep)
        {
            CheckerForPhysics = true;
        }
        else if (Player.transform.position.y < transform.position.y)
        {
            CheckerForPhysics = false;
        }

        if (CheckerForPhysics)
        {
            Physic.SetActive(true);
        }
        else
        {
            Physic.SetActive(false);
        }
    }
}
