﻿/* Это скрипт отслеживает нажатия на здания и кнопки меню зданий.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class Click_On_Buildings : MonoBehaviour
{
    //public bool is_main_window_active = true;
    // Переменная состояния главного окна, true - Открыто главное окно игры, false - открыто другое окно. 

    /*bool[] buildings_state; Массив состояний зданий 

    bool is_office_buy;
    bool is_nii_buy;
    bool is_svechki_buy;
    */

    private Tilemap map;
    private Camera main_camera;
    /*
    public TileBase TileToSet_Office;
    public TileBase TileBefore_Office;
    public TileBase TileToSet_NII;
    public TileBase TileBefore_NII;
    public TileBase TileToSet_Svechki;
    public TileBase TileBefore_Svechki;
    // Тайлы зданий

    [SerializeField] int price_nii;
    [SerializeField] int price_svechki;
    [SerializeField] int price_office;
    // Цены зданий

    public GameObject Office_Window;
    public GameObject NII_Window;
    public GameObject Svechki_Window;
    public GameObject Kikit_Window;
    // Диалоговые окна

    public GameObject Error_Window_Office;
    public GameObject Error_Window_Nii;
    public GameObject Error_Window_Svechki;
    // Всплывающие окна, указывающие на нехватку средств

    private GameObject message_object;
    */
    private GameObject main_parameters;
    
    void Start()
    {
        
        main_parameters = GameObject.Find("Main_Parameters");
        map = GetComponent<Tilemap>();
        main_camera = Camera.main;
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 click_position = main_camera.ScreenToWorldPoint(Input.mousePosition);
            Vector3Int cell_position = map.WorldToCell(click_position);
            
            if((main_parameters.GetComponent<Main_Parameters>().is_main_window_active == true) && (cell_position == new Vector3Int(9,8,-9) || cell_position == new Vector3Int(9, 8, -8) || cell_position == new Vector3Int(9, 9, -9) 
                || cell_position == new Vector3Int(10, 9, -8) || cell_position == new Vector3Int(10, 8, -9)))
            {
                main_parameters.GetComponent<Main_Parameters>().Office_Window.SetActive(true);

                GameObject.Find("Count_Students_Office/Count").GetComponent<Text>().text = main_parameters.GetComponent<Main_Parameters>().count_students_in_office.ToString();

                main_parameters.GetComponent<Main_Parameters>().is_main_window_active = false;
            }
            else if ((main_parameters.GetComponent<Main_Parameters>().is_main_window_active == true) && (cell_position == new Vector3Int(12, 12, -8) || cell_position == new Vector3Int(13, 13, -8) || cell_position == new Vector3Int(12, 12, -9) 
                || cell_position == new Vector3Int(10, 9, -8) || cell_position == new Vector3Int(13, 12, -9)))
            {
                main_parameters.GetComponent<Main_Parameters>().NII_Window.SetActive(true);

                GameObject.Find("Count_Students_NII/Count").GetComponent<Text>().text = main_parameters.GetComponent<Main_Parameters>().count_students_in_nii.ToString();

                main_parameters.GetComponent<Main_Parameters>().is_main_window_active = false;

            }
            else if ((main_parameters.GetComponent<Main_Parameters>().is_main_window_active == true) && (cell_position == new Vector3Int(6, 14, -9) || cell_position == new Vector3Int(7, 15, -9) || cell_position == new Vector3Int(7, 15, -8) || cell_position == new Vector3Int(10, 16, -8)
                || cell_position == new Vector3Int(7, 13, -9) || cell_position == new Vector3Int(8, 14, -9) || cell_position == new Vector3Int(8, 14, -8) || cell_position == new Vector3Int(9, 15, -8)))
            {
                main_parameters.GetComponent<Main_Parameters>().Svechki_Window.SetActive(true);

                GameObject.Find("Count_Students_Svechki/Count").GetComponent<Text>().text = main_parameters.GetComponent<Main_Parameters>().all_students.ToString() + " / " + main_parameters.GetComponent<Main_Parameters>().count_students_in_svechki_all.ToString();

                main_parameters.GetComponent<Main_Parameters>().is_main_window_active = false;

            }
            else if ((main_parameters.GetComponent<Main_Parameters>().is_main_window_active == true) && (cell_position == new Vector3Int(5, 10, -9) || cell_position == new Vector3Int(5, 10, -8)))
            {
                main_parameters.GetComponent<Main_Parameters>().Kikit_Window.SetActive(true);

                //GameObject.Find("Count_Students_Office/Count").GetComponent<Text>().text = main_parameters.GetComponent<Main_Parameters>()..ToString();

                main_parameters.GetComponent<Main_Parameters>().is_main_window_active = false;

            }
        }
    }
    public void swap_image_buildings(TileBase first_tile, TileBase second_tile)
    {
        map.SwapTile(first_tile, second_tile);
    }

}
