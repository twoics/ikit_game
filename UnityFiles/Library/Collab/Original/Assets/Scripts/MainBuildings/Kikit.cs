﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Kikit : Buildings
{
    private float IncrementCount;
    [SerializeField] Text IncrementCountField;
    [SerializeField] Text BorderRetingText;
    [SerializeField] GameObject IncrementGameObject;

    private float BorderRetingForAbility;
    public Kikit(GameObject Window) : base(Window)
    {

    }
    
    void Awake()
    {
        BorderRetingForAbility = PlayerPrefs.GetFloat("BorderRetingForAbility", 1000);
        //Read Workers from file
        ReadTeachersFromFile("ViewedTeachersKikit", "KikitTeachers");

        // Deserialization NII after load
        DeserializationKikit();
    }
    public void AwakeingAbility()
    {
        IncrementGameObject.SetActive(true);
        IncrementCount = PlayerPrefs.GetFloat("IncrementCount", 0.1f);
        IncrementCountField.text = "x" + IncrementCount.ToString();
        BorderRetingText.text = "Необходимое количество рейтинга для улучшения: \n\n" + BorderRetingForAbility.ToString();

    }

    public void StartMiniGame()
    {
        IsMainWindowActive = true;
        PlayerPrefs.Save();
        BuildingWindow.SetActive(false);
        SceneManager.LoadScene("Jumper");
    }

    public void Save()
    {
        PlayerPrefs.SetInt("StudentsCountKikit", count_students);
        PlayerPrefs.SetInt("LevelKikit", building_level);
        if (is_building_buy == true)
        {
            PlayerPrefs.SetInt("IsKikitBuy", 1);
        }
    }

    public void AcceptIncrementUpdate()
    {
        if (RetingCount > BorderRetingForAbility )
        {
            MoneyDelta += (int)(AllStudents * IncrementCount);
            IncrementCount += 0.01f;
            PlayerPrefs.SetFloat("IncrementCount", IncrementCount);

            BorderRetingForAbility += 1000;
            PlayerPrefs.SetFloat("BorderRetingForAbility", 1000);

            IncrementGameObject.SetActive(false);
        }
        else
            StartCoroutine(Show_Error_Window(ErrorWindow, "Не хватет рейтинга для улучшения"));
    }

    private void DeserializationKikit()
    {
        if (PlayerPrefs.GetInt("IsKikitBuy") == 1)
            is_building_buy = true;
        else
            is_building_buy = false;

        building_level = PlayerPrefs.GetInt("LevelKikit");
        count_students = PlayerPrefs.GetInt("StudentsCountKikit");

        if (is_building_buy)
        {
            UpdateMainGameAfterLoad();
        }
    }
}
