﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Linq;

public class Office : Buildings
{
    int ImpactOnMoney;
    int ImpactOnStudents;
    // Param for Special Ability

    public Office(GameObject Window) : base(Window)
    {

    }
    
    void Awake()
    {
        Debug.Log("+");
        // For Special Ability
        ImpactOnMoney = PlayerPrefs.GetInt("OfficeAbilityMoney", Random.Range(-1000, -2000));
        ImpactOnStudents = PlayerPrefs.GetInt("OfficeAbilityStudents", Random.Range(100, 300));

        // Read data from file
        ReadTeachersFromFile("ViewedTeacherOffice", "OfficeTeachers");

        // Deserialization Office after load
        DeserializationOffice();

    }

    public void Save()
    {
        PlayerPrefs.SetInt("StudentsCountOffice", count_students);
        PlayerPrefs.SetInt("LevelOffice", building_level);
        if (is_building_buy == true)
        {
            PlayerPrefs.SetInt("IsOfficeBuy", 1);
        }
    }
    
    public void SpecialAbility()
    {
        GameObject.Find("SpecialAbility/Money/Impact").GetComponent<Text>().text = ImpactOnMoney.ToString();
        GameObject.Find("SpecialAbility/Students/Impact").GetComponent<Text>().text = ImpactOnStudents.ToString();

    }
    
    public void ClickOnAcceptSpecial()
    {
        if (CountMoney + ImpactOnMoney < 0)
        {
            StartCoroutine(Show_Error_Window(ErrorWindow, "Не хватает денег для оплаты рекламы"));

        }
        else if (AllStudents + ImpactOnStudents > СapacitySvechki)
        {
            StartCoroutine(Show_Error_Window(ErrorWindow, "Не хватает места, чтобы поместить студентов в общежитие"));

        }
        else
        {
            CountMoney += ImpactOnMoney;
            count_students += ImpactOnStudents;
            AllStudents += ImpactOnStudents;

            GameObject.Find("Main_Parameters").GetComponent<Main_Parameters>().UpdateMainParameters();
            BuildingWindow.transform.Find("Count_Students").gameObject.GetComponentInChildren<Text>().text = count_students.ToString();

            ImpactOnMoney += Random.Range(-500, -1500);
            ImpactOnStudents += Random.Range(10, 60);

            PlayerPrefs.SetInt("OfficeAbilityMoney", ImpactOnMoney);
            PlayerPrefs.SetInt("OfficeAbilityStudents", ImpactOnStudents);
        }
    }

    private void DeserializationOffice()
    {
        if (PlayerPrefs.GetInt("IsOfficeBuy") == 1)
            is_building_buy = true;

        else
            is_building_buy = false;

        building_level = PlayerPrefs.GetInt("LevelOffice");
        count_students = PlayerPrefs.GetInt("StudentsCountOffice");

        if (is_building_buy)
        {
            UpdateMainGameAfterLoad();
        }
    }

}
