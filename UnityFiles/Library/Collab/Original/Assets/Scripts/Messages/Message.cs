﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;

public class Message : MonoBehaviour
{
    struct SomeMessage
    {
        public string TypeMessage;
        // Type message can be: "Info", "Choice","Story"
        // Info - Simple info message; can affect on some parameters, without user choice
        // Choice - User can choice - Accept deal, Reject deal
        // Story - User can choice - Accept Reject deal; After accept or reject will be a consequence

        public string TextMessage;


        public int ImpactOnMoney;
        public int ImpactOnReting;
        public int ImpactOnTrustRating;

        //Only For type message is a Story
        public string ConsequenceText;
        public int TimeDuration;
        
        public SomeMessage(string Type, string Text, int DeltaMoney, int DeltaReting, int DeltaTrustRaing = 0, string Consequence = null, int Duration = 0)
        /*
        Initialization
            :param Type: Type of message
            :param Text: Text messaage
            :param DeltaMoney: Impact on count money after accept
            :param DeltaReting: Impact on count reting after accept
            :param Consequence: The message that will be sent after accepting a message of type "Story"
            :param Duration:  Duration before sent Consequence
            :param DeltaTrustRaing: On this value change Trust Rating
         */
        {
            this.TypeMessage = Type;
            this.TextMessage = Text;
            this.ImpactOnMoney = DeltaMoney;
            this.ImpactOnReting = DeltaReting;
            this.ConsequenceText = Consequence;
            this.TimeDuration = Duration;
            this.ImpactOnTrustRating = DeltaTrustRaing;
        }
    }

    private int NumberViewedMessage;
    // Index of last Accepted message

    float CoolDownMessage = 0;
    bool IsMessageReceived = false;

    public Main_Parameters main_Parameters;

    [SerializeField] GameObject MessageWindow;

    [SerializeField] Text InfoField;
    [SerializeField] Text OrderField;
    [SerializeField] Text PlotMessageField;

    [SerializeField] GameObject InfoMenu;
    [SerializeField] GameObject SelectionMenu;
    [SerializeField] GameObject MenuWithEffects;

    [SerializeField] GameObject CloseButton;
    [SerializeField] GameObject ErrorWindow;

    //Sprites of message button
    [SerializeField] Sprite DefaultMessage;
    [SerializeField] Sprite MessageCame;

    // Show on which param will be effect 
    [SerializeField] Sprite TrustRetingIcon;
    [SerializeField] Sprite RetingIcon;

    // This sprite will be change on TrustSprite or RetingSprite (Which > 0)
    [SerializeField] GameObject MainSprite;

    Animator animator;

    private Queue<SomeMessage> MessageQueue = new Queue<SomeMessage>();

    public void SwapMessagePicture(string MessageName = null)
    {
        if (MessageName == "message_came")
        {
            gameObject.GetComponent<Image>().sprite = DefaultMessage;
        }
        else
        {
            gameObject.GetComponent<Image>().sprite = MessageCame;
        }
    }
    
    public void CloseWindow()
    {
        SwapMessagePicture("message_came");

        MessageWindow.GetComponent<Animator>().SetBool("IsCloseButtonPressed", true);
        StartCoroutine(main_Parameters.DelayForCloseWindow(MessageWindow));
    }

    private void ReadDataFromFile()
    {
        // Param for message
        string TypeTmp;
        string TextTmp;
        int DeltaMon;
        int DeltaRep;
        int DeltaTrus;
        string TextEffect;
        int DurationTime;


        TextAsset MessagesTxt = (TextAsset)Resources.Load("Messages", typeof(TextAsset));
        int tmpInt = 0;
        NumberViewedMessage = PlayerPrefs.GetInt("NumberViewedMessage", 0);
        foreach (string tmp in MessagesTxt.text.Split('\n'))
        {
            SomeMessage NewMessage;
            if(tmpInt >= NumberViewedMessage)
            // Read only messages that haven't been read
            {
                string[] ArrayString = tmp.Split(';');

                //As soon as posible refactor this shit

                // Create a message and add it to the queue
                (TypeTmp, TextTmp, DeltaMon, DeltaRep) = (ArrayString[0], ArrayString[1], Convert.ToInt32(ArrayString[2]), Convert.ToInt32(ArrayString[3]));
                if (TypeTmp == "Info")
                {
                    
                    NewMessage = new SomeMessage(TypeTmp, TextTmp, DeltaMon, DeltaRep);
                    MessageQueue.Enqueue(NewMessage);

                }
                else if (TypeTmp == "Choice")
                {
                    DeltaTrus = Convert.ToInt32(ArrayString[4]);
                    NewMessage = new SomeMessage(TypeTmp, TextTmp, DeltaMon, DeltaRep, DeltaTrus);
                    MessageQueue.Enqueue(NewMessage);

                }
                else
                {
                    (TextEffect, DurationTime)
                       = (ArrayString[4], Convert.ToInt32(ArrayString[5]));
                    NewMessage = new SomeMessage(TypeTmp, TextTmp, DeltaMon, DeltaRep, Consequence: TextEffect, Duration: DurationTime);
                    MessageQueue.Enqueue(NewMessage);
                }
            }
            tmpInt++;

        }
    }

    public void Awakining()
    {
        // Alex Kikit 1.06.2021 (╮°-°)╮┳━━┳ (╯°-°)╯┻━━┻


        MessageWindow.SetActive(true);
        main_Parameters.IsMainWindowActive = false;

        // Show message
        if (MessageQueue.Count > 0 && CoolDownMessage == 0)
        {
            SomeMessage TmpMessage = MessageQueue.Peek();
            if (TmpMessage.TypeMessage == "Info")
            {
                InfoMenu.SetActive(true);
                InfoField.text = TmpMessage.TextMessage;
                CloseButton.SetActive(false);

            }
            else if(TmpMessage.TypeMessage == "Choice")
            {
                SelectionMenu.SetActive(true);

                if (TmpMessage.ImpactOnReting > 0)
                {
                    MainSprite.GetComponent<Image>().sprite = RetingIcon;
                    GameObject.Find("AcceptOrder/ImpactOnReting").GetComponent<Text>().text = TmpMessage.ImpactOnReting.ToString();
                    SetColorText(GameObject.Find("AcceptOrder/ImpactOnReting"), TmpMessage.ImpactOnReting);
                }    

                else
                {
                    MainSprite.GetComponent<Image>().sprite = TrustRetingIcon;
                    GameObject.Find("AcceptOrder/ImpactOnReting").GetComponent<Text>().text = TmpMessage.ImpactOnTrustRating.ToString();
                    SetColorText(GameObject.Find("AcceptOrder/ImpactOnReting"), TmpMessage.ImpactOnTrustRating);

                }

                OrderField.text = TmpMessage.TextMessage;

                GameObject.Find("AcceptOrder/ImpactOnMoney").GetComponent<Text>().text = TmpMessage.ImpactOnMoney.ToString();
                SetColorText(GameObject.Find("AcceptOrder/ImpactOnMoney"), TmpMessage.ImpactOnMoney);

            }
            else
            {
                MenuWithEffects.SetActive(true);
                PlotMessageField.text = TmpMessage.TextMessage;
            }
        }
    }

    public void AcceptMessage()
    {
        IsMessageReceived = true;
        CoolDownMessage = Random.Range(5f, 10f);

        // Increase the number of viewed messages by 1, and write it in PlayerPrefs
        NumberViewedMessage++;
        PlayerPrefs.SetInt("NumberViewedMessage", NumberViewedMessage);

        SomeMessage TmpMessage = MessageQueue.Dequeue();

        // Update parameters after accept message
        if (TmpMessage.TypeMessage == "Info")
        {
            if (main_Parameters.CountMoney + TmpMessage.ImpactOnMoney <= 0)
                main_Parameters.CountMoney = 0;
            else
                main_Parameters.CountMoney += TmpMessage.ImpactOnMoney;

            if (main_Parameters.RetingCount + TmpMessage.ImpactOnReting <= 0)
                main_Parameters.RetingCount = 0;
            else
                main_Parameters.RetingCount += TmpMessage.ImpactOnReting;

            GameObject.Find("Main_Parameters").GetComponent<Main_Parameters>().UpdateMainParameters();
            CloseButton.SetActive(true);
            InfoMenu.SetActive(false);
            CloseWindow();
        }
        else if (TmpMessage.TypeMessage == "Choice")
        {
            if (main_Parameters.CountMoney + TmpMessage.ImpactOnMoney <= 0 || main_Parameters.RetingCount + TmpMessage.ImpactOnReting <= 0)
                StartCoroutine(Show_Error_Window(ErrorWindow, "Не хватает средств"));
            else
            {
                if (TmpMessage.ImpactOnReting > 0)
                {
                    main_Parameters.RetingCount += TmpMessage.ImpactOnReting;

                }
                else
                {
                    main_Parameters.RetingTrust += TmpMessage.ImpactOnTrustRating;
                }
            }

            main_Parameters.CountMoney += TmpMessage.ImpactOnMoney;

            GameObject.Find("Main_Parameters").GetComponent<Main_Parameters>().UpdateMainParameters();
            SelectionMenu.SetActive(false);
            CloseWindow();
        }
        
        
        else
        {   
            StartCoroutine(AddMessageAfterDuration(TmpMessage.TimeDuration, TmpMessage.ImpactOnMoney, TmpMessage.ImpactOnReting, TmpMessage.ConsequenceText));
            MenuWithEffects.SetActive(false);
            CloseWindow();

        }
    }

    public void RejectMessage()
    {
        IsMessageReceived = true;

        SomeMessage TmpMessage = MessageQueue.Dequeue();
        CoolDownMessage = Random.Range(10f, 15f);

        if (TmpMessage.TypeMessage == "Info")
            InfoMenu.SetActive(false);
        else if(TmpMessage.TypeMessage == "Choice")
            SelectionMenu.SetActive(false);
        else
            MenuWithEffects.SetActive(false);
        CloseWindow();

    }
    public IEnumerator AddMessageAfterDuration(int TimeDuration, int ImpactMoney, int ImpactReting, string TextMessage)
    {    
    /*
        Add info message in queue

        :param TimeDuration: Duration before add message
        :param ImpactMoney: Impact on money after read message
        :param ImpactReting: Impact on reting after read message
        :param TextMessage: Text message
    */
        SomeMessage message = new SomeMessage("Info", TextMessage, ImpactMoney, ImpactReting);

        yield return new WaitForSeconds(TimeDuration);

        MessageQueue.Enqueue(message);
    }

    private void SetColorText(GameObject gameObject, int value)
    {
        if (value > 0)
        {
            gameObject.GetComponent<Text>().color = new Color(0, 0.7f, 0);
        }
        else if (value < 0)
        {
            gameObject.GetComponent<Text>().color = new Color(0.7f, 0, 0);
        }
        else
        {
            gameObject.GetComponent<Text>().color = new Color(0.4f, 0.4f, 0.4f);
        }
    }
    private IEnumerator Show_Error_Window(GameObject ErrorWindow, string message)
    {
        /*
            Show Error window on 2 seconds

            :param ErrorWindow: Some Window what need show 
            :param message: Text on this Error Window
         */
        ErrorWindow.SetActive(true);
        ErrorWindow.GetComponentInChildren<Text>().text = message;
        yield return new WaitForSeconds(2f);
        ErrorWindow.SetActive(false);
    }
    private void Start()
    {
        ReadDataFromFile();

        animator = GetComponent<Animator>();

    }
    
    private void Update()
    {
        if(IsMessageReceived == true)
        {
            CoolDownMessage -= Time.deltaTime;
            if (CoolDownMessage <= 0)
            {
                IsMessageReceived = false;

                CoolDownMessage = 0;
            }
        }
        else
        {
            if (MessageQueue.Count > 0)
            {
                SwapMessagePicture();
            }
        }
    }
}
