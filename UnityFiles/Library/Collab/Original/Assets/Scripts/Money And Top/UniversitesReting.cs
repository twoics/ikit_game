﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;

public class UniversitesReting : MonoBehaviour
{
    public struct University
    {
        public string NameUniversity;
        public int RetingUniversity;

        public University(string UniversityName, int UniversityReting)
        {
            this.NameUniversity = UniversityName;
            this.RetingUniversity = UniversityReting;
        }
    }

    [SerializeField] Text FirstPlaceName;
    [SerializeField] Text SecondPlaceName;
    [SerializeField] Text ThirdPlaceName;

    [SerializeField] Text FirstPlaceRetingCount;
    [SerializeField] Text SecondPlaceRetingCount;
    [SerializeField] Text ThirdPlaceRetingCount;

    [SerializeField] Text AntoherPlaceNumber;
    [SerializeField] Text AntoherPlaceRetingCount;
    [SerializeField] GameObject AntoherPlaceObject;

    [SerializeField] GameObject UniversitiesRetingWindow;

    Main_Parameters main_Parameters;

    List<University> Universities = new List<University>();

    private float CoolDownUpdate = 50f;

    void Start()
    {
        main_Parameters = GameObject.Find("Main_Parameters").gameObject.GetComponent<Main_Parameters>();

        TextAsset UniversityTxt = (TextAsset)Resources.Load("Universites", typeof(TextAsset));
        foreach (string tmp in UniversityTxt.text.Split('\n'))
        {
            Universities.Add(DeserializationUniversitesFromTxt(tmp));
        }
        
        University SIB_FU = new University("СФУ", (int)main_Parameters.RetingCount);
        Universities.Add(SIB_FU);
    }

    University DeserializationUniversitesFromTxt(string TmpString)
    {

        string[] ArrayStrings = TmpString.Split(';');
        string UniversityName = ArrayStrings[0];
        int UniversityReting = Convert.ToInt32(ArrayStrings[1]);
        UniversityReting = PlayerPrefs.GetInt(UniversityName, UniversityReting);
        University TmpUniversity = new University(UniversityName, UniversityReting);
        return TmpUniversity;
    }
    
    public void Aweaking()
    {
        main_Parameters.ShowGameWindow(UniversitiesRetingWindow);
        UpdateRetingCounUniversities(true);
        CheckMainUniversityReting();
    }

    public void CloseRetingWindow()
    {
        UniversitiesRetingWindow.GetComponent<Animator>().SetTrigger("IsCloseWindow");
        StartCoroutine(main_Parameters.DelayForCloseWindow(UniversitiesRetingWindow));
    }

    public void CheckMainUniversityReting()
    {
        /*
            Sorts and displays university top
        */
        Universities.Sort((p2, p1) => p1.RetingUniversity.CompareTo(p2.RetingUniversity));

        FirstPlaceName.text = Universities[0].NameUniversity;
        FirstPlaceRetingCount.text = Universities[0].RetingUniversity.ToString();

        SecondPlaceName.text = Universities[1].NameUniversity;
        SecondPlaceRetingCount.text = Universities[1].RetingUniversity.ToString();

        ThirdPlaceName.text = Universities[2].NameUniversity;
        ThirdPlaceRetingCount.text = Universities[2].RetingUniversity.ToString();

        // If SFU rating is lower than the last one, show an additional line
        if ((int)main_Parameters.RetingCount < Universities[2].RetingUniversity)
        {
            AntoherPlaceObject.SetActive(true);
            AntoherPlaceNumber.text = (FindIndexOfSibFuUniversity() + 1).ToString();
            AntoherPlaceRetingCount.text = ((int)main_Parameters.RetingCount).ToString();
        }
        else
            AntoherPlaceObject.SetActive(false);

    }
    
    private int FindIndexOfSibFuUniversity()
    {
        for(int i = 0; i < Universities.Count; i++)
        {
            if (Universities[i].NameUniversity == "СФУ")
                return i;
        }
        return -1;
    }
    
    private void UpdateRetingCounUniversities(bool IsUpdateOnlySFU)
    {
        // Someday fix this shit (◕‿◕)
        int NewRatingCount;
        
        // This condition is called only when Reting Window opening, that necessary for correct work, becouse Top Reting update only once in "n" seconds, and sfu rating can change in these "n" seconds
        if (IsUpdateOnlySFU)
        {
            NewRatingCount = (int)main_Parameters.RetingCount;
            int SFU_Index = FindIndexOfSibFuUniversity();
            University TmpUniversity = new University(Universities[SFU_Index].NameUniversity, NewRatingCount);
            Universities.RemoveAt(SFU_Index);
            Universities.Insert(SFU_Index, TmpUniversity);
            return;
        }

        for (int i = 0; i < Universities.Count; i++)
        {
            // If University isn't SFU, means to Rating add Random value
            if (Universities[i].NameUniversity != "СФУ")
                NewRatingCount = Universities[i].RetingUniversity + Random.Range(-100, 350);
            // If University is SFU, means Rating = Rating SFU
            else
                NewRatingCount = (int)main_Parameters.RetingCount;

            University TmpUniversity = new University(Universities[i].NameUniversity, NewRatingCount);
            Universities.RemoveAt(i);
            Universities.Insert(i, TmpUniversity);
            SaveData(TmpUniversity);
        }
    }

    private void SaveData(University TmpUniversity)
    {
        // Becouse SFU Reting in MainParameters
        if(TmpUniversity.NameUniversity != "СФУ")
            PlayerPrefs.SetInt(TmpUniversity.NameUniversity, TmpUniversity.RetingUniversity);
    }

    void Update()
    {
        CoolDownUpdate -= Time.deltaTime;
        if (CoolDownUpdate <= 0)
        {
            UpdateRetingCounUniversities(false);
            CoolDownUpdate = 50f;

        }
    }
}
