﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyClicker : MonoBehaviour
{
    private Main_Parameters main_Parameters;
    private int CountClick;

    private float CoolDown;
    [SerializeField] Text CoolDownText;
    [SerializeField] GameObject CoolDownObject;
    void Start()
    {
        main_Parameters = GameObject.Find("Main_Parameters").GetComponent<Main_Parameters>();
        CountClick = PlayerPrefs.GetInt("CountClick", 0);
        CoolDown = PlayerPrefs.GetFloat("CoolDown", 50f);
    }


    public void OnButtonClick()
    {
        if(CountClick < 50)
        {
            main_Parameters.CountMoney += Random.Range(100, 350);
            GameObject.Find("Main_Parameters").GetComponent<Main_Parameters>().UpdateMainParameters();
            CountClick++;
            PlayerPrefs.SetInt("CountClick", CountClick);

        }
    }

    private void Update()
    {
        if(CountClick >= 50)
        {
            CoolDownObject.SetActive(true);
            CoolDown -= Time.deltaTime;
            CoolDownText.text = ((int)CoolDown).ToString();

            PlayerPrefs.SetFloat("CoolDown", CoolDown);

            if (CoolDown <= 0)
            {
                CountClick = 0;
                CoolDown = 50f;
                CoolDownObject.SetActive(false);
            }
        }
    }
}
